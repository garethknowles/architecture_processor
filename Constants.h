/*
 * Constants.h
 *
 *  Created on: Dec 13, 2012
 *      Author: GSK
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

enum ProcessorMode {
	BasicNoPipe, BasicScalar, BasicSuperScalar, SuperScalarReOrder
};

const bool demoMode = true;
const bool verboseAssembly = false;

#endif /* CONSTANTS_H_ */
