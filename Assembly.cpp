/*
 * Assembly.cpp
 *
 *  Created on: Nov 8, 2012
 *      Author: GSK
 */

#include "Assembly.h"

Assembly::Assembly() {

}

Assembly::~Assembly() {
}

vector<Instruction*> Assembly::assemblyToInstructions(istream *input,
		bool verbose) {

	vector<Instruction*> ins = vector<Instruction*>();
	vector<LabelLookup> labels = vector<LabelLookup>();
	int i = 1;

	// Get First Line
	while (!input->eof()) {
		string line = "";
		getline(*input, line);
		if (line.length() == 0) {
			i++;
			continue;
		}
		// Prepare Output
		stringstream out;
		out << "Line " << i << " Instruction: " << ins.size() << " ";

		if (!checkForPrefix(line, "\t")) {
			if (line.at(line.size() - 1) == ':') {
				// Label, add to list!
				LabelLookup label = LabelLookup();
				label.address = ins.size();
				line.erase(line.size() - 1, 1);
				label.label = line;
				labels.push_back(label);
				out << "LABEL name: " << label.label << " address: "
						<< label.address;
			} else {
				out << "Error: Not An Instruction or Label";
			}
		} else {
			// HAS TAB CHAR SO IS AN INSTRUCTION
			line.erase(0, 1);
			if (line.length() == 0) {
				i++;
				continue;
			}

			InstructionData *data = extractInstructionDataFromString(line);

			if (data->typeName.compare("CMP") == 0) {
				RegistersInstruction *compare = new RegistersInstruction(
						COMPARE, data->rd, data->rs, data->rt);
				handleConditions(compare, data);
				ins.push_back(compare);
				out << "COMPARE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " rt = "
						<< data->rt << " (rd=(rs==rt?0:(rs<rt?-1:1)))";
			} else if (data->typeName.compare("CMI") == 0
					|| data->typeName.compare("CMPI") == 0) {
				ImmediateInstruction *comparei = new ImmediateInstruction(
						COMPARE_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(comparei, data);
				ins.push_back(comparei);
				out << "COMPARE_IMMEDIATE cond = " << data->condition
						<< " rd = " << data->rd << " rs = " << data->rs
						<< " immediate = " << data->immediate
						<< " (rd=(rs==I?0:(rs<I?-1:1)))";
			} else if (data->typeName.compare("ADD") == 0) {
				RegistersInstruction *add = new RegistersInstruction(ADD,
						data->rd, data->rs, data->rt);
				handleConditions(add, data);
				ins.push_back(add);
				out << "ADD cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " rt = " << data->rt
						<< " (rd=rs+rt)";
			} else if (data->typeName.compare("ADDI") == 0) {
				ImmediateInstruction *addi = new ImmediateInstruction(
						ADD_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(addi, data);
				ins.push_back(addi);
				out << "ADD_IMMEDIATE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (rd=rs+I)";
			} else if (data->typeName.compare("SUB") == 0) {
				RegistersInstruction *sub = new RegistersInstruction(SUB,
						data->rd, data->rs, data->rt);
				handleConditions(sub, data);
				ins.push_back(sub);
				out << "SUBTRACT cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " rt = "
						<< data->rt << " (rd=rs-rt)";
			} else if (data->typeName.compare("SUBI") == 0) {
				ImmediateInstruction *subi = new ImmediateInstruction(
						SUB_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(subi, data);
				ins.push_back(subi);
				out << "SUBTRACT_IMMEDIATE cond = " << data->condition
						<< " rd = " << data->rd << " rs = " << data->rs
						<< " I = " << data->immediate << " (rd=rs-I)";
			} else if (data->typeName.compare("MUL") == 0) {
				RegistersInstruction *mul = new RegistersInstruction(MUL,
						data->rd, data->rs, data->rt);
				handleConditions(mul, data);
				ins.push_back(mul);
				out << "MULTIPLY cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " rt = "
						<< data->rt << " (rd=rs*rt)";
			} else if (data->typeName.compare("MULA") == 0) {
				RegistersInstruction *mula = new RegistersInstruction(
						MUL_ACCUMULATOR, 0, data->rd, data->rs);
				handleConditions(mula, data);
				ins.push_back(mula);
				out << "MULTIPLY_ACCUMULATOR cond = " << data->condition
						<< " rs = " << data->rd << " rt = " << data->rs
						<< " (accu=rs*rt)";
			} else if (data->typeName.compare("DIV") == 0) {
				RegistersInstruction *div = new RegistersInstruction(DIV, 0,
						data->rd, data->rs);
				handleConditions(div, data);
				ins.push_back(div);
				out << "DIVIDE cond = " << data->condition << " rs = "
						<< data->rd << " rt = " << data->rs
						<< " (accu=rs/rt|rs%rt)";

			} else if (data->typeName.compare("SL") == 0) {
				RegistersInstruction *shift_left = new RegistersInstruction(
						SHIFT_LEFT, data->rd, data->rs, data->rt);
				handleConditions(shift_left, data);
				ins.push_back(shift_left);
				out << "SHIFT LEFT cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " rt = "
						<< data->rt << " (rd=rs<<rt)";

			} else if (data->typeName.compare("SR") == 0) {
				RegistersInstruction *shift_right = new RegistersInstruction(
						SHIFT_RIGHT, data->rd, data->rs, data->rt);
				handleConditions(shift_right, data);
				ins.push_back(shift_right);
				out << "SHIFT RIGHT cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " rt = "
						<< data->rt << " (rd=rs>>rt)";
			} else if (data->typeName.compare("SLI") == 0) {
				ImmediateInstruction *shift_left = new ImmediateInstruction(
						SHIFT_LEFT_IMMEDIATE, data->rd, data->rs,
						data->immediate);
				handleConditions(shift_left, data);
				ins.push_back(shift_left);
				out << "SHIFT LEFT IMMEDIATE cond = " << data->condition
						<< " rd = " << data->rd << " rs = " << data->rs
						<< " imm = " << data->immediate << " (rd=rs<<I)";

			} else if (data->typeName.compare("SRI") == 0) {
				ImmediateInstruction *shift_right = new ImmediateInstruction(
						SHIFT_RIGHT_IMMEDIATE, data->rd, data->rs,
						data->immediate);
				handleConditions(shift_right, data);
				ins.push_back(shift_right);
				out << "SHIFT RIGHT IMMEDIATE cond = " << data->condition
						<< " rd = " << data->rd << " rs = " << data->rs
						<< " imm = " << data->immediate << " (rd=rs>>I)";
			} else if (data->typeName.compare("CLR") == 0) {
				RegistersInstruction *clr = new RegistersInstruction(CLEAR,
						data->rd, 0, 0);
				handleConditions(clr, data);
				ins.push_back(clr);
				out << "CLEAR cond = " << data->condition << " rd = "
						<< data->rd << " (rd=0)";
			} else if (data->typeName.compare("MOV") == 0) {
				RegistersInstruction *add = new RegistersInstruction(MOVE,
						data->rd, data->rs, 0);
				handleConditions(add, data);
				ins.push_back(add);
				out << "MOVE cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " (rd=rs)";
			} else if (data->typeName.compare("LDR") == 0) {
				ImmediateInstruction *ldr = new ImmediateInstruction(LOAD,
						data->rd, data->rs, data->immediate);
				handleConditions(ldr, data);
				ins.push_back(ldr);
				out << "LOAD cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " I = " << data->immediate
						<< " (rd=mem[rs+I])";
			} else if (data->typeName.compare("LDRI") == 0) {
				ImmediateInstruction *ldri = new ImmediateInstruction(
						LOAD_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(ldri, data);
				ins.push_back(ldri);
				out << "LOAD_IMMEDIATE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (rd=I)";
			} else if (data->typeName.compare("STR") == 0) {
				ImmediateInstruction *str = new ImmediateInstruction(STORE,
						data->rd, data->rs, data->immediate);
				handleConditions(str, data);
				ins.push_back(str);
				out << "STORE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (mem[rd+I]=r_s)";
			} else if (data->typeName.compare("STRI") == 0) {
				ImmediateInstruction *stri = new ImmediateInstruction(
						STORE_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(stri, data);
				ins.push_back(stri);
				out << "STORE_IMMEDIATE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (mem[rd]=I)";
			} else if (data->typeName.compare("MFHI") == 0) {
				RegistersInstruction *mvhi = new RegistersInstruction(
						MOVE_FROM_HIGH, data->rd, 0, 0);
				handleConditions(mvhi, data);
				ins.push_back(mvhi);
				out << "MOVE_FROM_HIGH cond = " << data->condition << " rd = "
						<< data->rd << " (rd=HI)";
			} else if (data->typeName.compare("MFLO") == 0) {
				RegistersInstruction *mvlo = new RegistersInstruction(
						MOVE_FROM_LOW, data->rd, 0, 0);
				handleConditions(mvlo, data);
				ins.push_back(mvlo);
				out << "MOVE_FROM_LOW cond = " << data->condition << " rd = "
						<< data->rd << " (rd=LO)";
			} else if (data->typeName.compare("AND") == 0) {
				RegistersInstruction *andd = new RegistersInstruction(AND,
						data->rd, data->rs, data->rt);
				handleConditions(andd, data);
				ins.push_back(andd);
				out << "AND cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " rt = " << data->rt
						<< " (rd=rs&rt)";
			} else if (data->typeName.compare("ANDI") == 0) {
				ImmediateInstruction *andi = new ImmediateInstruction(
						AND_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(andi, data);
				ins.push_back(andi);
				out << "AND_IMMEDIATE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (rd=rs&I)";
			} else if (data->typeName.compare("OR") == 0) {
				RegistersInstruction *orr = new RegistersInstruction(OR,
						data->rd, data->rs, data->rt);
				handleConditions(orr, data);
				ins.push_back(orr);
				out << "OR cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " rt = " << data->rt
						<< " (rd=rs|rt)";
			} else if (data->typeName.compare("ORI") == 0) {
				ImmediateInstruction *ori = new ImmediateInstruction(
						OR_IMMEDIATE, data->rd, data->rs, data->immediate);
				handleConditions(ori, data);
				ins.push_back(ori);
				out << "OR_IMMEDIATE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (rd=rs|I)";
			} else if (data->typeName.compare("EOR") == 0) {
				RegistersInstruction *eor = new RegistersInstruction(
						EXCLUSIVE_OR, data->rd, data->rs, data->rt);
				handleConditions(eor, data);
				ins.push_back(eor);
				out << "XOR cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " rt = " << data->rt
						<< " (rd=rs^rt)";
			} else if (data->typeName.compare("EORI") == 0) {
				ImmediateInstruction *eori = new ImmediateInstruction(
						EXCLUSIVE_OR_IMMEDIATE, data->rd, data->rs,
						data->immediate);
				handleConditions(eori, data);
				ins.push_back(eori);
				out << "XOR_IMMEDIATE cond = " << data->condition << " rd = "
						<< data->rd << " rs = " << data->rs << " I = "
						<< data->immediate << " (rd=rs^I)";
			} else if (data->typeName.compare("NOT") == 0) {
				RegistersInstruction *nott = new RegistersInstruction(NOT,
						data->rd, data->rs, 0);
				handleConditions(nott, data);
				ins.push_back(nott);
				out << "NOT cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " (rd=~rs)";
			} else if (data->typeName.compare("NOR") == 0) {
				RegistersInstruction *nor = new RegistersInstruction(NOR,
						data->rd, data->rs, data->rt);
				handleConditions(nor, data);
				ins.push_back(nor);
				out << "NOR cond = " << data->condition << " rd = " << data->rd
						<< " rs = " << data->rs << " rt = " << data->rt
						<< " (rd=~rs|~rt)";
			} else if (data->typeName.compare("B") == 0) {
				AddressInstruction *branch = new AddressInstruction(BRANCH,
						data->label);
				handleConditions(branch, data);
				ins.push_back(branch);
				out << "BRANCH cond = " << data->condition << " label = "
						<< data->label << " (pc=label)";
			} else if (data->typeName.compare("BR") == 0
					|| data->typeName.compare("J") == 0) {
				ImmediateInstruction *branchreg = new ImmediateInstruction(
						BRANCH_REGISTER, data->rd, 0, data->immediate);
				handleConditions(branchreg, data);
				ins.push_back(branchreg);
				out << "BRANCH_REGISTER cond = " << data->condition << " rd = "
						<< data->rd << " I = " << data->immediate
						<< " (pc=rd+I)";
			} else if (data->typeName.compare("BL") == 0) {
				// Branch Link & Push
				AddressInstruction *branchLink = new AddressInstruction(
						BRANCH_LINK, data->label);
				handleConditions(branchLink, data);
				ins.push_back(branchLink);
				out << "BRANCH LINK & PUSH cond = " << data->condition
						<< " label = " << data->label
						<< " (push stack, pc = label)";
			} else if (data->typeName.compare("BX") == 0) {
				// Branch Back & Pop
				AddressInstruction *branchBack = new AddressInstruction(
						BRANCH_BACK, "");
				handleConditions(branchBack, data);
				ins.push_back(branchBack);
				out << "BRANCH BACK & POP cond = " << data->condition
						<< " (pop stack, pc=return)";
			} else if (data->typeName.compare("NOP") == 0) {
				out << "NOP - Ignored ";
			} else if (data->typeName.compare("PRINTR") == 0) {
				ImmediateInstruction *print_reg = new ImmediateInstruction(
						PRINT_REG, 0, 0, 0);
				out << "PRINT REG ";
				ins.push_back(print_reg);
			} else if (data->typeName.compare("PRINTM") == 0) {
				out << "PRINT MEM ";
				ImmediateInstruction *print_mem = new ImmediateInstruction(
						PRINT_MEM, 0, 0, 0);
				ins.push_back(print_mem);
			} else if (data->typeName.compare("PRINT") == 0) {
				out << "PRINT MSG ";
				AddressInstruction *print_mem = new AddressInstruction(
						PRINT_MSG, data->label);
				ins.push_back(print_mem);
			} else if (data->typeName.compare("HA") == 0
					|| data->typeName.compare("HALT") == 0
					|| data->typeName.compare("STOP") == 0
					|| data->typeName.compare("END") == 0) {
				out << "HALT PROGRAM";
				Instruction *halt = new Instruction(HALT);
				ins.push_back(halt);
			} else {
				// Unknown Command
				out << "Unknown Command \"" << line << "\"";
			}
		}

		// Output If on Verbose Mode
		if (verbose) {
			cout << out.str() << endl;
		}

		i++;
	}

	// Replace Labels
	for (unsigned int x = 0; x < ins.size(); x++) {
		Instruction *instruction = ins.at(x);
		if (instruction->type == BRANCH || instruction->type == BRANCH_LINK) {
			string lab = ((AddressInstruction *) instruction)->label;
			for (unsigned int y = 0; y < labels.size(); y++) {
				LabelLookup labLookup = labels.at(y);
				if (labLookup.label.compare(lab) == 0) {
					// MATCH! Fill in Address
					((AddressInstruction *) instruction)->address =
							labLookup.address;
				}
			}
		}
	}

	return ins;
}

bool Assembly::checkForPrefix(std::string const& lhs, std::string const& rhs) {
	return std::equal(lhs.begin(),
			lhs.begin() + std::min(lhs.size(), rhs.size()), rhs.begin());
}

void Assembly::handleConditions(Instruction *ins, InstructionData *data) {
	ins->conditionalOnEQ = false;
	ins->conditionalOnLT = false;
	ins->contitionalOnGT = false;
	ins->contitionalOnNE = false;
	ins->conditional = true;
	switch (data->condition) {
	case (ConditionNotEqual):
		ins->contitionalOnNE = true;
		break;
	case (ConditionEqual):
		ins->conditionalOnEQ = true;
		break;
	case (ConditionLessThanOrEqual):
		ins->conditionalOnEQ = true;
		ins->conditionalOnLT = true;
		break;
	case (ConditionGreaterThanOrEqual):
		ins->conditionalOnEQ = true;
		ins->contitionalOnGT = true;
		break;
	case (ConditionLessThan):
		ins->conditionalOnLT = true;
		break;
	case (ConditionGreaterThan):
		ins->contitionalOnGT = true;
		break;
	case (ConditionAlways):
	default:
		ins->conditional = false;
		break;
	}
}

conditionType Assembly::checkForConditionType(string * input) {
	conditionType cond = ConditionAlways;
	if (input->size() > 1) {
		string lastTwoChars = input->substr(input->size() - 2, 2);
		if (lastTwoChars.compare("EQ") == 0) {
			cond = ConditionEqual;
		} else if (lastTwoChars.compare("NE") == 0) {
			cond = ConditionNotEqual;
		} else if (lastTwoChars.compare("GE") == 0) {
			cond = ConditionGreaterThanOrEqual;
		} else if (lastTwoChars.compare("LE") == 0) {
			cond = ConditionLessThanOrEqual;
		} else if (lastTwoChars.compare("GT") == 0) {
			cond = ConditionGreaterThan;
		} else if (lastTwoChars.compare("LT") == 0) {
			cond = ConditionLessThan;
		} else if (lastTwoChars.compare("AL") == 0) {
			cond = ConditionAlways;
		} else {
			return cond;
		}
		input->erase(input->size() - 2, 2);
		return cond;
	}

	return cond;
}

InstructionData* Assembly::extractInstructionDataFromString(string input) {

	InstructionData *data = new InstructionData();
	data->rd = 0, data->rs = 0, data->rt = 0, data->immediate = 0;
	int regsDone = 0;

// Split by spaces
	stringstream buffer(input);
	vector<string> split;
	copy(istream_iterator<string>(buffer), istream_iterator<string>(),
			back_inserter(split));

// Get Condition and Remove from type string if it exsits:
	data->condition = checkForConditionType(&split[0]);
// Set the Instruction Type Name
	data->typeName = split[0];

	if (split.size() == 2) {
// Has Address
		data->label = split[1];

// Just incase MFHI/MFLO with 1 input
		if (split[1].size() > 1
				&& (split[1].at(0) == 'R' || (split[1]).at(0) == 'r')) {
			stringstream buffer(split[1].substr(1, split[1].size() - 1));
			int regPos;
			buffer >> regPos;
			data->rd = regPos;
		}
	} else {
// Has Registers or Immediate values
		for (int i = 1; (unsigned) i < split.size(); i++) {
			if (split[i].at(0) == 'R' || (split[i]).at(0) == 'r') {
				// Register Val
				stringstream buffer(split[i].substr(1, split[i].size() - 1));
				int regPos;
				buffer >> regPos;
				if (regsDone == 0)
					data->rd = regPos;
				else if (regsDone == 1)
					data->rs = regPos;
				else if (regsDone == 2)
					data->rt = regPos;
				regsDone++;
			} else if (split[i].at(0) == '#') {
				// Immediate with # prefix
				stringstream buffer(split[i].substr(1, split[i].size() - 1));
				int immVal;
				buffer >> immVal;
				data->immediate = immVal;
			} else {
				// Treat as immediate without prefix
				stringstream buffer(split[i]);
				int immVal;
				buffer >> immVal;
				data->immediate = immVal;
			}
		}
	}

	return data;
}
