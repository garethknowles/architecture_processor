/*
 * WritebackUnit.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#include "WritebackUnit.h"

WritebackUnit::WritebackUnit(RegisterFile *registers, MemoryFile *memory,
		Pipeline *pipeline) {
	this->reg = registers;
	this->mem = memory;
	this->pipe = pipeline;
	currentInstruction = NULL;
}

WritebackUnit::~WritebackUnit() {
}

bool WritebackUnit::doWriteBackStage() {

	this->currentInstruction = this->pipe->getWriteIns();
	// Do Nothing if no instruction
	if (!currentInstruction)
		return false;

	//cout << "WriteBack: " << currentInstruction->pcAtFetch << " ISSUE " << currentInstruction->issuedNumber << endl;

	// DEBUG PRINT INSTRUCTIONS
	if (currentInstruction->type == PRINT_REG)
		reg->printReg();
	else if (currentInstruction->type == PRINT_MEM)
		mem->printMem();
	else if (currentInstruction->type == PRINT_MSG)
		cout << ((AddressInstruction *) currentInstruction)->label << endl;

	// For re-order buffer mode, halt stops here
	if (currentInstruction->type == HALT)
		return true;

	if (currentInstruction->updateAcc) {
		reg->setAccu(currentInstruction->accu);
		pipe->resStation->updateAcc(currentInstruction->accu);
	}
	if (currentInstruction->updateMem) {
		mem->setVal(currentInstruction->store_pos,
				currentInstruction->store_val);
		pipe->resStation->updateMemory(currentInstruction->store_pos,
				currentInstruction->store_val);
	}
	if (currentInstruction->updateReg) {
		reg->setVal(currentInstruction->store_pos,
				currentInstruction->store_val);
		pipe->resStation->updateReg(currentInstruction->store_pos,
				currentInstruction->store_val);
	}
	if (currentInstruction->type == COMPARE
			|| currentInstruction->type == COMPARE_IMMEDIATE) {
		reg->setFlags(currentInstruction->setNotEqualFlag,
				currentInstruction->setGreaterThanFlag,
				currentInstruction->setLessThanFlag);
		pipe->resStation->updateConditions();
	}
	if (currentInstruction->type == BRANCH_LINK) {
		// Push Vals To Stack
		reg->pushToCallStack(currentInstruction->pcAtFetch);
		pipe->resStation->updateCallStack();
	}
	if (currentInstruction->type == BRANCH_BACK) {
		currentInstruction->store_val = reg->popFromCallStack() + 1;
	}
	if (currentInstruction->updatePC) {
		pipe->clearPreExecute();
		reg->setProgramCounter(currentInstruction->store_val);
		pipe->resStation->updateReg(31, reg->getProgramCounter());
		if (pipe->reOrderUnit != NULL) {
			pipe->reOrderUnit->resetCount(pipe->instructionCount);
			pipe->clearPipe();
		}
	}



//	reg->printReg();
//	mem->printMem();
	return false;
}

void WritebackUnit::doWriteBackStageNoPipe() {

	// Do Nothing if no instruction
	if (!currentInstruction)
		return;

	//cout << "WriteBack: " << currentInstruction->pcAtFetch << endl;

	// DEBUG PRINT INSTRUCTIONS
	if (currentInstruction->type == PRINT_REG)
		reg->printReg();
	else if (currentInstruction->type == PRINT_MEM)
		mem->printMem();
	else if (currentInstruction->type == PRINT_MSG)
		cout << ((AddressInstruction *) currentInstruction)->label << endl;

	if (currentInstruction->updateAcc) {
		reg->setAccu(currentInstruction->accu);
		//pipe->resStation->updateAcc(currentInstruction->accu);
	}
	if (currentInstruction->updateMem) {
		mem->setVal(currentInstruction->store_pos,
				currentInstruction->store_val);
		//pipe->resStation->updateMemory();
	}
	if (currentInstruction->updateReg) {
		reg->setVal(currentInstruction->store_pos,
				currentInstruction->store_val);
		//pipe->resStation->updateReg(currentInstruction->store_pos,
		//currentInstruction->store_val);
	}
	if (currentInstruction->type == COMPARE
			|| currentInstruction->type == COMPARE_IMMEDIATE) {
		reg->setFlags(currentInstruction->setNotEqualFlag,
				currentInstruction->setGreaterThanFlag,
				currentInstruction->setLessThanFlag);
		//pipe->resStation->updateConditions();
	}
	if (currentInstruction->type == BRANCH_LINK) {
		// Push Vals To Stack
		reg->pushToCallStack(currentInstruction->pcAtFetch);
		//pipe->resStation->updateCallStack();
		//pipe->resStation->updateReg(31, reg->getProgramCounter());
		//pipe->clearPreExecute();
	}
	if (currentInstruction->type == BRANCH_BACK) {
		currentInstruction->store_val = reg->popFromCallStack() + 1;
		//pipe->resStation->updateReg(31, reg->getProgramCounter());
		//pipe->clearPreExecute();
	}
	if (currentInstruction->updatePC) {
		if (currentInstruction->store_pos == 0) {
			reg->setProgramCounter(currentInstruction->store_val - 1);
			//pipe->resStation->updateReg(31, reg->getProgramCounter());
			////pipe->clearPreExecute();
		} else {
			reg->incrementProgramCounter(currentInstruction->store_val - 1);
			//pipe->resStation->updateReg(31, reg->getProgramCounter());
			//pipe->clearPreExecute();
		}
	}
//	reg->printReg();
//	mem->printMem();
}

