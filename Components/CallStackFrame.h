/*
 * CallStackFrame.h
 *
 *  Created on: Nov 7, 2012
 *      Author: GSK
 */

#ifndef CALLSTACKFRAME_H_
#define CALLSTACKFRAME_H_

#include <vector>

using namespace std;

class CallStackFrame {
public:
	int returnAddress;
	vector<int> savedRegisters;
	CallStackFrame(int add, vector<int> regs);
	virtual ~CallStackFrame();
};

#endif /* CALLSTACKFRAME_H_ */
