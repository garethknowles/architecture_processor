/*
 * CallStackFrame.cpp
 *
 *  Created on: Nov 7, 2012
 *      Author: GSK
 */

#include "CallStackFrame.h"



CallStackFrame::CallStackFrame(int add, vector<int> regs) {
	returnAddress = add;
	for (unsigned i = 1; i < 21; i++) {
		savedRegisters.push_back(regs[i]);
	}
}

CallStackFrame::~CallStackFrame() {
}

