/*
 * MemoryFile.h
 *
 *  Created on: Nov 7, 2012
 *      Author: GSK
 */

#ifndef MEMORYFILE_H_
#define MEMORYFILE_H_

#include <iostream>
#include <vector>

using namespace std;

class MemoryFile {
public:
	vector<int> memValues;

	MemoryFile(int size);
	virtual ~MemoryFile();

	int getVal(int i);
	void setVal(int i, int val);
	void printMem();
};

#endif /* MEMORYFILE_H_ */
