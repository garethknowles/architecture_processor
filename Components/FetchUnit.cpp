/*
 * FetchUnit.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#include "FetchUnit.h"

FetchUnit::FetchUnit(RegisterFile *registers, Pipeline *pipeline) {
	this->reg = registers;
	this->pipe = pipeline;
	currentInstruction = NULL;
}

FetchUnit::~FetchUnit() {
}



void FetchUnit::doFetchStage() {

	currentInstruction = NULL;

	if (pipe->resStation->registersLock[31] == true) {
		//cout << "no fetch due to pc lock" << endl;
		return;
	}

	if ((reg->getProgramCounter() >= 0)
			&& ((unsigned)reg->getProgramCounter()
					< pipe->instructionList->size())){
		SetInstruction(pipe->instructionList->at(reg->getProgramCounter()));
	}


	// Do Nothing if no instruction
	if (!currentInstruction)
		return;


	currentInstruction->issuedNumber = pipe->instructionCount;
	pipe->instructionCount = pipe->instructionCount+1;
	currentInstruction->pcAtFetch = reg->getProgramCounter();

	reg->incrementProgramCounter();

	//cout << "Fetched PC: " << currentInstruction->pcAtFetch << "with issue: " << currentInstruction->issuedNumber << endl;

	// Put Instruction onto Decode Queue
	pipe->pushOntoDecodeQueue(currentInstruction);
}

void FetchUnit::doFetchStageNoPipe() {

	// Do Nothing if no instruction
	if (!currentInstruction)
		return;

	currentInstruction->pcAtFetch = reg->getProgramCounter();

	//cout << "Fetched PC: " << currentInstruction->pcAtFetch << endl;
	// Put Instruction onto Decode Queue
	//this->pipe->pushOntoDecodeQueue(currentInstruction);
}


void FetchUnit::SetInstruction(Instruction *in) {
	if (in == NULL) {
		this->currentInstruction = NULL;
		return;
	}
	// Need to copy instruction rather than just use
	switch (in->instructionMode) {
	case 0:
		this->currentInstruction = new ImmediateInstruction((ImmediateInstruction *)in);
		break;
	case 1:
		this->currentInstruction = new RegistersInstruction((RegistersInstruction *)in);
		break;
	case 2:
		this->currentInstruction = new AddressInstruction((AddressInstruction *)in);
		break;
	case -1:
	default:
		this->currentInstruction = new Instruction(in);
		break;
	}
}


