/*
 * RegisterFile.h
 *
 *  Created on: Nov 7, 2012
 *      Author: GSK
 */

#ifndef REGISTERFILE_H_
#define REGISTERFILE_H_

#include "CallStackFrame.h"
#include <iostream>
#include <vector>

using namespace std;

class RegisterFile {
public:
	bool greaterThanFlag;
	bool lessThanFlag;
	bool notEqualFlag;
	long int accumulator;
	vector<int> regValues;
	vector<CallStackFrame> callStack;

	RegisterFile();
	virtual ~RegisterFile();

	void incrementProgramCounter(int val);
	void incrementProgramCounter();
	int getProgramCounter();
	void setProgramCounter(int val);

	void pushToCallStack(int pcAtFetch);
	int popFromCallStack();

	long int getAccu();
	void setAccu(int long val);

	int getVal(int i);
	void setVal(int i, int val);

	void setFlags(bool NE, bool GT, bool LT);
	bool getNotEqualFlag();
	bool getGreaterThanFlag();
	bool getLessThanFlag();
	bool getEqualFlag();

	void printReg();
};

#endif /* REGISTERFILE_H_ */

