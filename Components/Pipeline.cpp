/*
 * Pipeline.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#include "Pipeline.h"

Pipeline::Pipeline(vector<Instruction*> *insList) {
	instructionList = insList;
	writeQueue = list<Instruction*>();
	conditionCheckQueue = list<Instruction*>();
	executeQueue = list<Instruction*>();
	decodeQueue = list<Instruction*>();
	writeQueue = list<Instruction*>();
	reOrderQueue = vector<Instruction*>();
	resStation = NULL;
	clearPipe();
	instructionCount = 0;
	reOrderUnit = NULL;
}

Pipeline::~Pipeline() {

}

void Pipeline::updateReservationStation() {
	if (reOrderUnit != NULL) {
		// We have Re-order, can do more than just front
		vector<Instruction *> out = this->resStation->updateWithReorder();
		for (unsigned i = 0; i < out.size(); i++) {
			//cout << "Taking a value out of Res Station " << out.at(i)->pcAtFetch << " " << i << "/" << out.size() << endl;
			this->executeQueue.push_back(out.at(i));
		}
	} else {
		// Do Normal Without reOrderUnit
		Instruction *out = this->resStation->update();
		while (out != NULL) {
			this->executeQueue.push_back(out);
			out = this->resStation->update();
		}
	}
}

void Pipeline::pushOntoDecodeQueue(Instruction *ins) {
	this->decodeQueue.push_back(ins);
}

void Pipeline::pushOntoReservationStation(Instruction *ins) {
	this->resStation->pushInstruction(ins);
}

void Pipeline::pushOntoExecuteQueue(Instruction *ins) {
	this->executeQueue.push_back(ins);
}

void Pipeline::pushOntoReOrderQueue(Instruction *ins) {
	this->reOrderQueue.push_back(ins);
}

void Pipeline::pushOntoWriteQueue(Instruction *ins) {
	this->writeQueue.push_back(ins);
}

void Pipeline::clearPreExecute() {
	//cout << "clear Pre Exe" << endl;
	decodeQueue.clear();
	if (resStation != NULL)
		resStation->clear();
	executeQueue.clear();
}

void Pipeline::clearPipe() {
	//cout << "clear whole pipe" << endl;
	decodeQueue.clear();
	if (resStation != NULL)
		resStation->clear();
	executeQueue.clear();
	reOrderQueue.clear();
	writeQueue.clear();
}

Instruction* Pipeline::getDecodeIns() {
	if (decodeQueue.empty())
		return NULL;
	Instruction *ins = decodeQueue.front();
	decodeQueue.pop_front();
	return ins;
}

Instruction* Pipeline::getExecuteIns() {
	if (executeQueue.empty())
		return NULL;
	Instruction *ins = executeQueue.front();
	executeQueue.pop_front();
	return ins;
}

Instruction* Pipeline::getWriteIns() {
	if (writeQueue.empty())
		return NULL;
	Instruction *ins = writeQueue.front();
	writeQueue.pop_front();
	return ins;
}

bool Pipeline::isEmpty() {
	if (executeQueue.empty() && decodeQueue.empty() && writeQueue.empty())
		return true;

	return false;
}
