/*
 * RegisterFile.cpp
 *
 *  Created on: Nov 7, 2012
 *      Author: GSK
 */

#include "RegisterFile.h"

RegisterFile::RegisterFile() {
	greaterThanFlag = false;
	lessThanFlag = false;
	notEqualFlag = false;
	accumulator = 0;
	regValues = vector<int>(32, 0);
	callStack = vector<CallStackFrame>();

}

RegisterFile::~RegisterFile() {
}

int RegisterFile::getVal(int i) {
	if (i == 31)
		return getProgramCounter();
	if ((unsigned)i >= regValues.size())
		return 0;
	return regValues[i];
}

void RegisterFile::setVal(int i, int val) {
	if (i == 0)
		return;
	if (i == 31) {
		setProgramCounter(val);
		return;
	}

	if ((unsigned)i >= regValues.size())
		return;
	regValues[i] = val;
}


long int RegisterFile::getAccu() {
	return accumulator;
}

void RegisterFile::setAccu(int long val) {
	accumulator = val;
}

int RegisterFile::getProgramCounter() {
	return regValues[31];
}

void RegisterFile::incrementProgramCounter() {
	regValues[31]++;
}

void RegisterFile::incrementProgramCounter(int val) {
	regValues[31]+=val;
}

void RegisterFile::setProgramCounter(int val) {
	regValues[31] = val;
}

void RegisterFile::pushToCallStack(int pcAtFetch) {
	CallStackFrame newFrame = CallStackFrame(pcAtFetch, regValues);
	callStack.push_back(newFrame);
}

int RegisterFile::popFromCallStack() {
	CallStackFrame frame = callStack.back();
	for (unsigned i = 1; i < 21; i++) {
		regValues[i] = frame.savedRegisters[i-1];
	}
	int returnAdd = frame.returnAddress;
	callStack.pop_back();
	return returnAdd;
}

void RegisterFile::setFlags(bool NE, bool GT, bool LT) {
	greaterThanFlag = GT;
	lessThanFlag = LT;
	notEqualFlag = NE;
}

bool RegisterFile::getNotEqualFlag() {
	return notEqualFlag;
}

bool RegisterFile::getGreaterThanFlag() {
	return greaterThanFlag;
}

bool RegisterFile::getLessThanFlag() {
	return lessThanFlag;
}

bool RegisterFile::getEqualFlag() {
	return !notEqualFlag;
}

void RegisterFile::printReg() {

	cout << "Printing Register Values:" << endl;

	for (unsigned i = 0; i < regValues.size(); i++) {
		int const val = regValues[i];
		cout << "r"<< i << ": " << val << ", ";
		if (i > 0 && i % 10 == 0 && i != regValues.size()-1)
			cout << endl;
	}
	cout << "GT:" << greaterThanFlag << " LT:" << lessThanFlag << " NE:" << notEqualFlag << endl;
}

