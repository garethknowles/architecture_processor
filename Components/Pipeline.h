/*
 * Pipeline.h
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#ifndef PIPELINE_H_
#define PIPELINE_H_

#include "../Instructions/ImmediateInstruction.h"
#include "../Instructions/RegistersInstruction.h"
#include "../Instructions/AddressInstruction.h"
#include "ReOrderUnit.h"
#include "ReservationStation.h"
#include <list>
#include <vector>

class ReOrderUnit;
class ReservationStation;

class Pipeline {
public:
	Pipeline(vector<Instruction*> *insList);
	virtual ~Pipeline();

	void pushOntoDecodeQueue(Instruction *ins);
	void pushOntoReservationStation(Instruction *ins);
	void pushOntoConditionCheckQueue(Instruction *ins);
	void pushOntoExecuteQueue(Instruction *ins);
	void pushOntoReOrderQueue(Instruction *ins);
	void pushOntoWriteQueue(Instruction *ins);

	void conditionsFail(Instruction* ins);

	void clearPreExecute();
	void clearPipe();

	void updateReservationStation();

	Instruction* getDecodeIns();
	Instruction* getExecuteIns();
	Instruction* getWriteIns();
	Instruction* getCheckConditionIns();

	vector<Instruction*> *instructionList;
	list<Instruction*> decodeQueue;
	list<Instruction*> conditionCheckQueue;
	list<Instruction*> executeQueue;
	list<Instruction*> writeQueue;
	vector<Instruction*> reOrderQueue;
	ReservationStation *resStation;
	ReOrderUnit *reOrderUnit;

	bool isEmpty();
	int instructionCount;
};

#endif /* PIPELINE_H_ */
