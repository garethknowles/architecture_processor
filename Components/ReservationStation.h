/*
 * ReservationStation.h
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#ifndef RESERVATIONSTATION_H_
#define RESERVATIONSTATION_H_

#include <vector>
#include "../Instructions/ImmediateInstruction.h"
#include "../Instructions/RegistersInstruction.h"
#include "../Instructions/AddressInstruction.h"
#include "ReOrderUnit.h"
#include "RegisterFile.h"
#include "MemoryFile.h"

class Pipeline;
class ReOrderUnit;

class ReservationStation {

public:
	class ReservationValue {
	public:
		Instruction *instruction;
		bool stackRead;
		bool accuRead;
		bool conditionRead;
		bool registersRead;
		bool memoryRead;
		int reg1Pos;
		int reg2Pos;
		bool stackWrite;
		bool accuWrite;
		bool conditionsWrite;
		bool registersWrite;
		bool memoryWrite;
		int regDPos;
		int memWritePos;
		int memReadPos;

		public:
			ReservationValue(Instruction *ins) {
				instruction = ins;
				stackRead = false;
				stackWrite = false;
				accuRead = false;
				accuWrite = false;
				conditionRead = false;
				registersRead = false;
				reg1Pos = 0;
				reg2Pos = 0;
				memoryRead = false;
				registersWrite = false;
				regDPos = 0;
				memoryWrite = false;
				conditionsWrite = false;
				memWritePos = 0;
				memReadPos = 0;

			}
	};

public:
	ReservationStation(RegisterFile *ireg, MemoryFile *mem);
	virtual ~ReservationStation();

	void clear();

	void pushInstruction(Instruction *ins);
	Instruction *update();
	vector<Instruction*> updateWithReorder();
	bool checkIfCanRelease(ReservationValue *val);
	bool checkNoLowerQueueWrites(ReservationValue *val);
	void performLocks(ReservationValue *val);
	void undoLocks(Instruction *ins);
	bool checkConditionals(Instruction *ins);

	void updateReg(int regPos, int val);
	void updateAcc(long val);
	void updateCallStack();
	void updateConditions();
	void updateMemory(int memPos, int memVal);

	bool conditionLock;
	bool *registersLock;
	bool accumalatorLock;
	bool callStackLock;
	vector<int> memoryValsLocked;
	RegisterFile *reg;
	MemoryFile *mem;
	vector<ReservationValue*> reservationList;
	ReOrderUnit *reOrderUnit;
};

#endif /* RESERVATIONSTATION_H_ */
