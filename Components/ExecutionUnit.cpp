/*
 * ExecutionUnit.cpp
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#include "ExecutionUnit.h"
#include <stdlib.h>

ExecutionUnit::ExecutionUnit(Pipeline *pipeline) {
	pipe = pipeline;
	currentInstruction = NULL;
}

ExecutionUnit::~ExecutionUnit() {

}


bool ExecutionUnit::doExecutionStage() {

	currentInstruction = this->pipe->getExecuteIns();


	// Do Nothing if no instruction
	if (!currentInstruction)
		return false;
	// Return true on HALT
	if (currentInstruction->type == HALT  && pipe->resStation == NULL) {
		return true;
	}

	//cout << "Executing PC: " << this->currentInstruction->pcAtFetch << endl;

	currentInstruction->execute();

	// Clear due to branch
//	if (currentInstruction->branch) {
//		this->pipe->clearPreExecute();
//	}
	// Put Instruction onto Write Queue
	if (pipe->reOrderUnit != NULL) {
		pipe->pushOntoReOrderQueue(currentInstruction);
	} else {
		this->pipe->pushOntoWriteQueue(currentInstruction);
	}
	return false;
}

bool ExecutionUnit::doExecutionStageNoPipe() {

	// Do Nothing if no instruction
	if (!currentInstruction)
		return false;
	// Return false on HALT
	if (currentInstruction->type == HALT) {
		return true;
	}

	//cout << "Executing PC: " << this->currentInstruction->pcAtFetch << endl;

	currentInstruction->execute();

	return false;
}


