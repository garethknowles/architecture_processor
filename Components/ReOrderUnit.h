/*
 * ReOrderUnit.h
 *
 *  Created on: Dec 14, 2012
 *      Author: GSK
 */

#ifndef REORDERUNIT_H_
#define REORDERUNIT_H_

#include "Pipeline.h"
#include "../Instructions/Instruction.h"
#include <list>
#include <algorithm>

class Pipeline;

class ReOrderUnit {
public:
	ReOrderUnit(Pipeline *pipeline);

	void doReOrderStage();
	bool checkInstructionsForRelease();
	void resetCount(int val);
	void addToIgnore(int val);

	Pipeline *pipe;
	int currentCount;
	list<int> ignores;
};

#endif /* REORDERUNIT_H_ */
