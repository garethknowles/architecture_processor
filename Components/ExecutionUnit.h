/*
 * ExecutionUnit.h
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */


#ifndef EXECUTIONUNIT_H_
#define EXECUTIONUNIT_H_

#include "../Instructions/ImmediateInstruction.h"
#include "../Instructions/RegistersInstruction.h"
#include "../Instructions/AddressInstruction.h"
#include "Pipeline.h"

class ExecutionUnit {
public:
	Instruction *currentInstruction;
	Pipeline *pipe;

	ExecutionUnit(Pipeline *pipeline);
	virtual ~ExecutionUnit();

	bool doExecutionStage();
	bool doExecutionStageNoPipe();
};

#endif /* EXECUTIONUNIT_H_ */
