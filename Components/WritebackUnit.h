/*
 * WritebackUnit.h
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#ifndef WRITEBACKUNIT_H_
#define WRITEBACKUNIT_H_

#include "../Instructions/ImmediateInstruction.h"
#include "../Instructions/RegistersInstruction.h"
#include "../Instructions/AddressInstruction.h"
#include "RegisterFile.h"
#include "MemoryFile.h"
#include "Pipeline.h"

class WritebackUnit {
public:
	Instruction *currentInstruction;
	RegisterFile *reg;
	MemoryFile *mem;
	Pipeline *pipe;

	WritebackUnit(RegisterFile *registers, MemoryFile *memory, Pipeline *pipeline);
	virtual ~WritebackUnit();

	bool doWriteBackStage();
	void doWriteBackStageNoPipe();
};

#endif /* WRITEBACKUNIT_H_ */
