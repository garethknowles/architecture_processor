/*
 * DecodeUnit.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#include "DecodeUnit.h"

DecodeUnit::DecodeUnit(RegisterFile *registers, MemoryFile *memory,
		Pipeline *pipeline) {
	this->reg = registers;
	this->mem = memory;
	this->pipe = pipeline;
	currentInstruction = NULL;
}

DecodeUnit::~DecodeUnit() {
}

void DecodeUnit::doDecodeStage() {

	currentInstruction = this->pipe->getDecodeIns();

	// Do Nothing if no instruction
	if (!currentInstruction)
		return;

	switch (currentInstruction->instructionMode) {
	case 0:
		((ImmediateInstruction *) currentInstruction)->register_s_val =
				reg->getVal(
						((ImmediateInstruction *) currentInstruction)->register_s_num);
		if (currentInstruction->type == LOAD) {
			currentInstruction->load_pos =
					((ImmediateInstruction *) currentInstruction)->register_s_val
							+ ((ImmediateInstruction *) currentInstruction)->immediate;
			int val = mem->getVal(currentInstruction->load_pos);
			((ImmediateInstruction *) currentInstruction)->load_val = val;
		}
		if (currentInstruction->type == STORE
				|| currentInstruction->type == BRANCH_REGISTER) {
			((ImmediateInstruction *) currentInstruction)->register_d_val =
					reg->getVal(
							((ImmediateInstruction *) currentInstruction)->register_d_num);
		}
		break;
	case 1:
		((RegistersInstruction *) currentInstruction)->register_s_val =
				reg->getVal(
						((RegistersInstruction *) currentInstruction)->register_s_num);
		((RegistersInstruction *) currentInstruction)->register_t_val =
				reg->getVal(
						((RegistersInstruction *) currentInstruction)->register_t_num);
		break;
	case 2:
		if (currentInstruction->type == BRANCH_LINK) {
		}
		if (currentInstruction->type == BRANCH_BACK) {
		}
		break;
	case -1:
	default:
		break;
	}

	//cout << "Decoded: " << currentInstruction->pcAtFetch << endl;

	// Put Instruction to Reservation Station
	this->pipe->pushOntoReservationStation(currentInstruction);

}

void DecodeUnit::doDecodeStageNoPipe() {

	// Do Nothing if no instruction
	if (!currentInstruction)
		return;

	switch (currentInstruction->instructionMode) {
	case 0:
		((ImmediateInstruction *) currentInstruction)->register_s_val =
				reg->getVal(
						((ImmediateInstruction *) currentInstruction)->register_s_num);
		if (currentInstruction->type == LOAD) {
			currentInstruction->load_pos =
					((ImmediateInstruction *) currentInstruction)->register_s_val
							+ ((ImmediateInstruction *) currentInstruction)->immediate;
			int val = mem->getVal(currentInstruction->load_pos);
			((ImmediateInstruction *) currentInstruction)->load_val = val;
		}
		if (currentInstruction->type == STORE
				|| currentInstruction->type == BRANCH_REGISTER) {
			((ImmediateInstruction *) currentInstruction)->register_d_val =
					reg->getVal(
							((ImmediateInstruction *) currentInstruction)->register_d_num);
		}
		break;
	case 1:
		((RegistersInstruction *) currentInstruction)->register_s_val =
				reg->getVal(
						((RegistersInstruction *) currentInstruction)->register_s_num);
		((RegistersInstruction *) currentInstruction)->register_t_val =
				reg->getVal(
						((RegistersInstruction *) currentInstruction)->register_t_num);
		break;
	case 2:
		if (currentInstruction->type == BRANCH_LINK) {
		}
		if (currentInstruction->type == BRANCH_BACK) {
		}
		break;
	case -1:
	default:
		break;
	}

	// Do Condtional Check
	//Check Conditionals
	if (currentInstruction->conditional) {
		// Check for Conditions
		bool CEQ = currentInstruction->conditionalOnEQ;
		bool CLT = currentInstruction->conditionalOnLT;
		bool CNE = currentInstruction->contitionalOnNE;
		bool CGT = currentInstruction->contitionalOnGT;
		bool fail = true;
		if (CEQ && reg->getEqualFlag())
			fail = false;
		if (CLT && reg->getLessThanFlag())
			fail = false;
		if (CNE && reg->getNotEqualFlag())
			fail = false;
		if (CGT && reg->getGreaterThanFlag())
			fail = false;

		if (fail) {
			currentInstruction = NULL;
		}
	}
}
