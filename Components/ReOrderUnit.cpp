/*
 * ReOrderUnit.cpp
 *
 *  Created on: Dec 14, 2012
 *      Author: GSK
 */

#include "ReOrderUnit.h"

ReOrderUnit::ReOrderUnit(Pipeline *pipeline) {
	currentCount = 0;
	pipe = pipeline;
	ignores = list<int>();
}

void ReOrderUnit::doReOrderStage() {
	// Nothing to do if re order queue empty
	if (pipe->reOrderQueue.empty())
		return;

	bool release = checkInstructionsForRelease();
	// Keep going till can't any more
	while (release) {
		release = checkInstructionsForRelease();
	}
}

bool ReOrderUnit::checkInstructionsForRelease() {

	// Check current value isn't on ignores list
	if (ignores.size() > 0) {
		int ignore = ignores.front();
		int current = this->currentCount;
		if (current == ignore) {
			currentCount++;
			ignores.pop_front();
			ignores.sort();
			return true;
		}
	}

	// Loop through Queue checking for value equal to current count
	for (unsigned i = 0; i < pipe->reOrderQueue.size(); i++) {
		Instruction *ins = pipe->reOrderQueue.at(i);
		if (ins->issuedNumber == currentCount) {
			// Found current count, increment and release!
			currentCount++;
			pipe->pushOntoWriteQueue(ins);
			pipe->reOrderQueue.erase(find(pipe->reOrderQueue.begin(), pipe->reOrderQueue.end(), ins));
			return true;
		}
	}
	return false;
}

void ReOrderUnit::resetCount(int val) {
	currentCount = val;
	// If Using reOrderQueue
	//cout << "reorder count now waiting for: " << currentCount << endl;
	ignores.clear();
	pipe->reOrderQueue.clear();
}

void ReOrderUnit::addToIgnore(int val) {
	ignores.push_back((int)val);
	ignores.sort();
}
