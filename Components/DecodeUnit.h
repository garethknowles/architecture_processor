/*
 * DecodeUnit.h
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#ifndef DECODEUNIT_H_
#define DECODEUNIT_H_

#include "../Instructions/ImmediateInstruction.h"
#include "../Instructions/RegistersInstruction.h"
#include "../Instructions/AddressInstruction.h"
#include "RegisterFile.h"
#include "MemoryFile.h"
#include "Pipeline.h"

class DecodeUnit {
public:
	Instruction *currentInstruction;
	RegisterFile *reg;
	MemoryFile *mem;
	Pipeline *pipe;

	DecodeUnit(RegisterFile *registers, MemoryFile *memory, Pipeline *pipeline);
	virtual ~DecodeUnit();

	void doDecodeStageNoPipe();
	void doDecodeStage();
};

#endif /* DECODEUNIT_H_ */
