/*
 * ReservationStation.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#include "ReservationStation.h"

ReservationStation::ReservationStation(RegisterFile *ireg, MemoryFile *imem) {
	conditionLock = false;
	registersLock = new bool[32]();
	for (int i = 0; i < 32; i++)
		registersLock[i] = false;
	accumalatorLock = false;
	callStackLock = false;
	memoryValsLocked = vector<int>();
	reservationList = vector<ReservationValue*>();
	reg = ireg;
	mem = imem;
	reOrderUnit = NULL;
}

ReservationStation::~ReservationStation() {
}

void ReservationStation::clear() {
	//cout << "clearing res station" << endl;
	reservationList.clear();
	conditionLock = false;
	registersLock = new bool[32]();
	for (int i = 0; i < 32; i++)
		registersLock[i] = false;
	accumalatorLock = false;
	callStackLock = false;
	memoryValsLocked.clear();
}

Instruction *ReservationStation::update() {

	if (reservationList.empty())
		return NULL;

	// If PC Locked, can't do any instructions!
	if (registersLock[31] == true) {
		return NULL;
	}

	// Try to Send first Value
	if (checkIfCanRelease(reservationList.front())) {
		// Can Release, Perform Lock and Release
		Instruction *instruction = reservationList.front()->instruction;
		if (checkConditionals(instruction)) {
			performLocks(reservationList.front());
			//cout << "releasing instruction to exe queue: " << instruction->pcAtFetch << endl;
			reservationList.erase(reservationList.begin());
			return instruction;
		} else {
			// Didn't Meeet Conditions, Check Next Value
			reservationList.erase(reservationList.begin());
			return update();
		}
	}

	return NULL;
}

vector<Instruction*> ReservationStation::updateWithReorder() {

	// Setup Vector to return values
	vector<Instruction *> outVals = vector<Instruction *>();

	if (reservationList.empty())
		return outVals;

	//cout << "Doing ResStat size currently: " << reservationList.size() << endl;

	// Try to send any value in queue that can be sent:
	vector<ReservationValue *> valsToRemove = vector<ReservationValue *>();

	for (unsigned i = 0; i < reservationList.size(); i++) {
		ReservationValue *val = reservationList.at(i);

		if (checkIfCanRelease(reservationList.at(i))) {
			// Check no values in the queue lower that it that will cause issues
			if (checkNoLowerQueueWrites(reservationList.at(i))) {
				// Can Release, Perform Lock and Release
				Instruction *instruction = val->instruction;
				if (checkConditionals(instruction)) {
					performLocks(val);
					//cout << "releasing instruction to exe queue: "
					//		<< instruction->pcAtFetch << endl;
					outVals.push_back(instruction);
				} else {
					// If there is a conditional in queue, we can just ignore this one and move on as if cannot release yet
					bool shouldBreak = false;
					for (unsigned j = 0; j < i; j++) {
						if (reservationList.at(j)->instruction->type == COMPARE
								|| reservationList.at(j)->instruction->type
										== COMPARE_IMMEDIATE) {
							if (reservationList.at(j)->instruction->issuedNumber
									< instruction->issuedNumber) {
								shouldBreak = true;
								break;
							}
						}
					}
					if (shouldBreak)
						continue;
					// As there isn't, throw it away
					reOrderUnit->addToIgnore(instruction->issuedNumber);
				}
				// Remove here to prevent being thown away before cond check
				valsToRemove.push_back(val);
			}
		}
	}

	for (unsigned j = 0; j < valsToRemove.size(); j++) {
		reservationList.erase(
				find(reservationList.begin(), reservationList.end(),
						valsToRemove.at(j)));
	}

	return outVals;
}

bool ReservationStation::checkConditionals(Instruction *ins) {
	//Check Conditionals
	if (ins->conditional) {
		// Check for Conditions
		bool CEQ = ins->conditionalOnEQ;
		bool CLT = ins->conditionalOnLT;
		bool CNE = ins->contitionalOnNE;
		bool CGT = ins->contitionalOnGT;
		bool fail = true;
		if (CEQ && reg->getEqualFlag())
			fail = false;
		if (CLT && reg->getLessThanFlag())
			fail = false;
		if (CNE && reg->getNotEqualFlag())
			fail = false;
		if (CGT && reg->getGreaterThanFlag())
			fail = false;

		if (fail) {
			return false;
		}
	}
	return true;
}
void ReservationStation::performLocks(ReservationValue *ins) {

	if (ins->stackWrite) {
		callStackLock = true;
	}
	if (ins->accuWrite) {
		accumalatorLock = true;
	}
	if (ins->conditionsWrite) {
		conditionLock = true;
	}
	if (ins->memoryWrite) {
		// reCalc storePos
		int storePos = 0;
		if (ins->instruction->type == STORE)
			storePos =
					((ImmediateInstruction*) ins->instruction)->register_d_val
							+ ((ImmediateInstruction*) ins->instruction)->immediate;
		if (ins->instruction->type == STORE_IMMEDIATE)
			storePos = ((ImmediateInstruction*) ins->instruction)->immediate;

		this->memoryValsLocked.push_back(storePos);
	}
	if (ins->registersWrite) {
		registersLock[ins->regDPos] = true;
	}
}

bool ReservationStation::checkNoLowerQueueWrites(ReservationValue *val) {

	for (unsigned i = 0; i < reservationList.size(); i++) {

		ReservationValue *listVal = reservationList.at(i);

		// CHECK READS - Check if another ins wants to write the value you want to read
		// Skip Values Issued Later Than Current
		if (listVal->instruction->issuedNumber
				>= val->instruction->issuedNumber)
			continue;

		// Check Condition Fillers Values
		if (val->conditionRead && listVal->conditionsWrite) {
			return false;
		}

		// Check Acc
		if (val->accuRead && listVal->accuWrite) {
			return false;
		}

		// Check Stack Locks
		if (val->stackRead && listVal->stackWrite) {
			return false;
		}

		// Check Memory Locks
		if (val->memoryRead && listVal->memoryWrite) {
			return false;
		}

		// Check Reg Locks
		if (val->registersRead && listVal->registersWrite) {
			if (listVal->regDPos == val->reg1Pos
					|| listVal->regDPos == val->reg2Pos)
				return false;
		}

		// CHECK WRITES - Check if another ins wants to read or write to value before you
		if (val->accuWrite)
			if (listVal->accuWrite || listVal->accuRead)
				return false;

		if (val->conditionsWrite)
			if (listVal->conditionsWrite || listVal->conditionRead)
				return false;

		if (val->stackWrite)
			if (listVal->stackWrite || listVal->stackRead)
				return false;

		if (val->memoryWrite)
			if (listVal->memoryWrite || listVal->memoryRead)
				return false;

		if (val->registersWrite) {
			if (listVal->registersWrite) {
				if (val->regDPos == listVal->regDPos)
					return false;
			}
			if (listVal->registersRead) {
				if (val->regDPos == listVal->reg1Pos)
					return false;
				if (val->regDPos == listVal->reg2Pos)
					return false;
			}
		}

	}

	return true;
}

bool ReservationStation::checkIfCanRelease(ReservationValue *val) {

// Check Condition Lock
	if (val->conditionRead && conditionLock)
		return false;

// Check Accu Lock
	if (val->accuRead && accumalatorLock)
		return false;

// Check Stack Lock
	if (val->stackRead && callStackLock)
		return false;

// Check Reg Lock
	if (val->registersRead) {
		if (registersLock[val->reg1Pos])
			return false;
		if (registersLock[val->reg2Pos])
			return false;
	}

// Check Memory Lock
	if (val->memoryRead) {
		if (find(memoryValsLocked.begin(), memoryValsLocked.end(),
				val->instruction->load_pos) != memoryValsLocked.end()) {
			return false;
		}
	}

// Passed All Lock Checks, So can Release
	return true;
}

void ReservationStation::pushInstruction(Instruction *ins) {

	ReservationValue *res = new ReservationValue(ins);

// Handle Conditional Read
	if (ins->conditional)
		res->conditionRead = true;

	if (ins->type == COMPARE || ins->type == COMPARE_IMMEDIATE) {
		res->conditionsWrite = true;
	}

// Handle Registers Read
	switch (ins->instructionMode) {
	case 0:
		// Immediate Instructions
		if (((ImmediateInstruction *) ins)->register_s_num != 0) {
			res->registersRead = true;
			res->reg1Pos = ((ImmediateInstruction *) ins)->register_s_num;
		}
		if (ins->type == LOAD) {
			// Special Case of Memory Load
			res->memoryRead = true;
		}
		if (ins->type == STORE || ins->type == BRANCH_REGISTER) {
			if (((ImmediateInstruction *) ins)->register_d_num != 0) {
				res->registersRead = true;
				res->reg2Pos = ((ImmediateInstruction *) ins)->register_d_num;
			}
		}
		if (ins->type == STORE || ins->type == STORE_IMMEDIATE) {
			// Stores don't save to reg D, writes mem instead
			res->memoryWrite = true;
		} else if (ins->type == BRANCH_REGISTER) {
			res->registersWrite = true;
			res->regDPos = 31;
			if (((ImmediateInstruction *) ins)->register_d_num != 0) {
				res->registersRead = true;
				res->regDPos = ((ImmediateInstruction *) ins)->register_d_num;
			}
		} else {
			// Not store - all others may save to reg
			if (((ImmediateInstruction *) ins)->register_d_num != 0) {
				res->registersWrite = true;
				res->regDPos = ((ImmediateInstruction *) ins)->register_d_num;
			}
		}
		break;
	case 1:
		// Register Instructions
		// Handle Accu Read
		if (ins->type == MOVE_FROM_HIGH || ins->type == MOVE_FROM_LOW) {
			res->accuRead = true;
		}

		if (((RegistersInstruction *) ins)->register_s_num != 0) {
			res->registersRead = true;
			res->reg1Pos = ((RegistersInstruction *) ins)->register_s_num;
		}
		if (((RegistersInstruction *) ins)->register_t_num != 0) {
			res->registersRead = true;
			res->reg2Pos = ((RegistersInstruction *) ins)->register_t_num;
		}

		if (ins->type == MUL_ACCUMULATOR || ins->type == DIV) {
			res->accuWrite = true;
		} else {
			if (((RegistersInstruction *) ins)->register_d_num != 0) {
				res->registersWrite = true;
				res->regDPos = ((RegistersInstruction *) ins)->register_d_num;
			}
		}
		break;
	case 2:
		res->registersWrite = true;
		res->regDPos = 31;
		if (ins->type == BRANCH_LINK) {
			res->stackWrite = true;
		}
		// Handle Call Stack Read
		if (ins->type == BRANCH_BACK)
			res->stackRead = true;
		break;
	default:
		break;
	}

	this->reservationList.push_back(res);
}

void ReservationStation::updateReg(int regPos, int val) {
// Remove Lock
	registersLock[regPos] = false;
//	if (regPos == 31)
//		cout << "unlocked pc" << endl;

	//cout << "unlocked " << regPos << endl;

	if (regPos == 0)
		return;
// Update Values in Reservation List
	for (unsigned i = 0; i < reservationList.size(); i++) {
		ReservationValue *res = reservationList.at(i);

		if (res->registersRead) {

			if (res->reg1Pos == regPos) {
				// Update Value
				if (res->instruction->instructionMode == 0) {
					((ImmediateInstruction *) res->instruction)->register_s_val =
							val;
				} else if (res->instruction->instructionMode == 1) {
					((RegistersInstruction *) res->instruction)->register_s_val =
							val;
				}
				// Handle Special Case - LOAD
				if (res->instruction->type == LOAD) {
					res->instruction->load_pos =
							((ImmediateInstruction *) res->instruction)->register_s_val
									+ ((ImmediateInstruction *) res->instruction)->immediate;
					// Need to get new mem value
					int val = mem->getVal(res->instruction->load_pos);
					((ImmediateInstruction *) res->instruction)->load_val = val;
				}
			}
			if (res->reg2Pos == regPos) {
				// Handle 2nd Reg of STORE - the RD Value
				if (res->instruction->type == STORE
						|| res->instruction->type == BRANCH_REGISTER) {
					((ImmediateInstruction *) res->instruction)->register_d_val =
							val;
				} else {
					((RegistersInstruction *) res->instruction)->register_t_val =
							val;
				}
			}
		}
	}
}

void ReservationStation::updateAcc(long val) {
// Remove Lock
	accumalatorLock = false;
// Update Values in Reservation List
	for (unsigned i = 0; i < reservationList.size(); i++) {
		ReservationValue *res = reservationList.at(i);
		if (res->accuRead) {
			if (res->instruction->type == MOVE_FROM_HIGH) {
				((RegistersInstruction *) res->instruction)->register_d_val =
						(int) (val >> 32);
			}
			if (res->instruction->type == MOVE_FROM_LOW) {
				((RegistersInstruction *) res->instruction)->register_d_val =
						(int) val;
			}
		}
	}
}

void ReservationStation::updateCallStack() {
// Remove Lock
	callStackLock = false;
}

void ReservationStation::updateConditions() {
	conditionLock = false;
}

void ReservationStation::updateMemory(int memPos, int memVal) {
//cout << "about to unlock " << memPos << endl;
	this->memoryValsLocked.erase(
			find(memoryValsLocked.begin(), memoryValsLocked.end(), memPos));

// Update Values in Reservation List
	for (unsigned i = 0; i < reservationList.size(); i++) {
		ReservationValue *res = reservationList.at(i);
		if (res->memoryRead) {
			if (res->instruction->load_pos == memPos) {
				((ImmediateInstruction *) res->instruction)->load_val = memVal;
			}
		}
	}
}
