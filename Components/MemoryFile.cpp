/*
 * MemoryFile.cpp
 *
 *  Created on: Nov 7, 2012
 *      Author: GSK
 */

#include "MemoryFile.h"

MemoryFile::MemoryFile(int size) {
	memValues = vector<int>(size, 0);
}

MemoryFile::~MemoryFile() {
}

int MemoryFile::getVal(int i) {
	if ((unsigned)i >= memValues.size())
		return 0;
	return memValues[i];
}

void MemoryFile::setVal(int i, int val) {
	if ((unsigned)i >= memValues.size())
		return;
	memValues[i] = val;
}

void MemoryFile::printMem() {

	cout << "Printing Memory Values:"  << endl;

	for (unsigned i = 0; i < memValues.size(); i++) {
		int const val = memValues[i];
		cout << "Mem[" << i << "]: " << val << ", ";
		if (i > 0 && i % 10 == 0 && i != memValues.size()-1)
			cout << endl;
	}
	cout << endl;
}
