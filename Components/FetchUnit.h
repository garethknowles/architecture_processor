/*
 * FetchUnit.h
 *
 *  Created on: Dec 11, 2012
 *      Author: GSK
 */

#ifndef FETCHUNIT_H_
#define FETCHUNIT_H_

#include "../Instructions/ImmediateInstruction.h"
#include "../Instructions/RegistersInstruction.h"
#include "../Instructions/AddressInstruction.h"
#include "RegisterFile.h"
#include "Pipeline.h"

class FetchUnit {
public:
	Instruction *currentInstruction;
	RegisterFile *reg;
	Pipeline *pipe;

	FetchUnit(RegisterFile *registers, Pipeline *pipeline);
	virtual ~FetchUnit();


	void doFetchStage();
	void doFetchStageNoPipe();

	void SetInstruction(Instruction *in);

};

#endif /* FETCHUNIT_H_ */
