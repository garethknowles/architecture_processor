/*
 * AddressInstruction.cpp
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#include "AddressInstruction.h"

AddressInstruction::AddressInstruction(instructionType type, string lab) :
		Instruction(type) {
	instructionMode = 2;
	label = lab;
	address = -1;
}

AddressInstruction::AddressInstruction(AddressInstruction *org) :
		Instruction(org) {
	instructionMode = org->instructionMode;
	label = org->label;
	address = org->address;
	issuedNumber = org->issuedNumber;
}

AddressInstruction::~AddressInstruction() {
}

void AddressInstruction::setAddress(int add) {
	address = add;
}

void AddressInstruction::execute() {

	switch (this->type) {
	// CONTROL FLOW
	case (BRANCH):
		store_val = address;
		store_pos = 0;
		updatePC = true;
		branch = true;
		break;
	case (BRANCH_LINK):
		store_val = address;
		store_pos = 0;
		updatePC = true;
		branch = true;
		break;
	case (BRANCH_BACK):
		updatePC = true;
		branch = true;
		break;
	case (NOP):
	default:
		break;
	}
}

