/*
 * RegistersInstruction.h
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#ifndef REGISTERSINSTRUCTION_H_
#define REGISTERSINSTRUCTION_H_

#include "Instruction.h"
#include <iostream>

using namespace std;

class RegistersInstruction: public Instruction {
public:
	unsigned short register_d_num;
	int register_d_val;
	unsigned short register_s_num;
	int register_s_val;
	unsigned short register_t_num;
	int register_t_val;

	RegistersInstruction(instructionType type, unsigned short rd, unsigned short rs, unsigned short rt);
	RegistersInstruction(RegistersInstruction *org);
	virtual ~RegistersInstruction();
	virtual void execute();

};

#endif /* REGISTERSINSTRUCTION_H_ */
