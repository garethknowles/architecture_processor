/*
 * Instruction.h
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_

enum instructionType {
	NOP,
	HALT,

	COMPARE,
	COMPARE_IMMEDIATE,  // 3

	ADD,
	ADD_IMMEDIATE,  //5
	SUB,
	SUB_IMMEDIATE,
	MUL,
	MUL_ACCUMULATOR,
	DIV, // 10
	CLEAR,
	SHIFT_LEFT,
	SHIFT_RIGHT,
	SHIFT_LEFT_IMMEDIATE,
	SHIFT_RIGHT_IMMEDIATE,  //15

	MOVE,
	LOAD,
	LOAD_IMMEDIATE,
	STORE,
	STORE_IMMEDIATE, // 20
	MOVE_FROM_HIGH,
	MOVE_FROM_LOW,

	AND,
	AND_IMMEDIATE,
	OR, // 25
	OR_IMMEDIATE,
	EXCLUSIVE_OR,
	EXCLUSIVE_OR_IMMEDIATE,
	NOR,
	NOT,  // 30

	BRANCH,
	BRANCH_REGISTER,
	BRANCH_LINK,
	BRANCH_BACK,

	PRINT_REG,  // 35
	PRINT_MEM,
	PRINT_MSG,
};

class Instruction {
public:
	instructionType type;
	int instructionMode;
	bool updateReg;
	bool updateMem;
	bool updateAcc;
	bool updatePC;
	bool setGreaterThanFlag;
	bool setLessThanFlag;
	bool setNotEqualFlag;
	bool conditionalOnLT;
	bool contitionalOnNE;
	bool contitionalOnGT;
	bool conditionalOnEQ;
	bool conditional;
	bool branch;
	int store_val;
	int store_pos;
	int load_val;
	int load_pos;
	long int accu;
	int pcAtFetch;
	int issuedNumber;

	Instruction(instructionType itype);
	Instruction(Instruction *org);
	virtual ~Instruction();
	virtual void execute();
};

#endif /* INSTRUCTION_H_ */
