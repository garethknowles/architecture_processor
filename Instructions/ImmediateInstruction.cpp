/*
 * ImmediateInstruction.cpp
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#include "ImmediateInstruction.h"

ImmediateInstruction::ImmediateInstruction(instructionType type,
		unsigned short rd, unsigned short rs, unsigned short im) :
		Instruction(type) {
	instructionMode = 0;
	register_s_num = rs;
	register_s_val = 0;
	register_d_num = rd;
	register_d_val = 0;
	immediate = im;
}

ImmediateInstruction::ImmediateInstruction(ImmediateInstruction *org) :
		Instruction(org) {
	instructionMode = org->instructionMode;
	register_s_num = org->register_s_num;
	register_s_val = org->register_s_val;
	register_d_num = org->register_d_num;
	register_d_val = org->register_d_val;
	immediate = org->immediate;
	issuedNumber = org->issuedNumber;
}

ImmediateInstruction::~ImmediateInstruction() {
}

void ImmediateInstruction::execute() {
	switch (this->type) {

	int cmp;

	// CONDITION
case (COMPARE_IMMEDIATE):
	cmp = (register_s_val == immediate ?
			0 : (register_s_val < immediate ? -1 : 1));
	setGreaterThanFlag = (cmp == 1 ? true : false);
	setLessThanFlag = (cmp == -1 ? true : false);
	setNotEqualFlag = (cmp == 0 ? false : true);
	if (register_d_num != 0) {
		store_pos = register_d_num;
		store_val = cmp;
		updateReg = true;
	}
	break;

	// ARITHMETIC
case (ADD_IMMEDIATE):
	register_d_val = register_s_val + immediate;
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;
case (SUB_IMMEDIATE):
	register_d_val = register_s_val - immediate;
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;
case (SHIFT_LEFT_IMMEDIATE):
	register_d_val = (int) (register_s_val << immediate);
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;
case (SHIFT_RIGHT_IMMEDIATE):
	register_d_val = (int) (register_s_val >> immediate);
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;

	// DATA TRANSFER
case (LOAD):
	store_pos = register_d_num;
	store_val = load_val;
	updateReg = true;
	break;
case (LOAD_IMMEDIATE):
	store_pos = register_d_num;
	store_val = immediate;
	updateReg = true;
	break;
case (STORE):
	store_val = register_s_val;
	store_pos = register_d_val + immediate;
	updateMem = true;
	break;
case (STORE_IMMEDIATE):
	store_val = immediate;
	store_pos = register_d_val;
	updateMem = true;
	break;

	// LOGICAL
case (AND_IMMEDIATE):
	register_d_val = register_s_val & immediate;
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;
case (OR_IMMEDIATE):
	register_d_val = register_s_val | immediate;
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;
case (EXCLUSIVE_OR_IMMEDIATE):
	register_d_val = register_s_val ^ immediate;
	store_val = register_d_val;
	store_pos = register_d_num;
	updateReg = true;
	break;

	// CONTROL FLOW
case (BRANCH_REGISTER):
	store_val = register_d_val + immediate;
	store_pos = 0;
	updatePC = true;
	branch = true;
	break;

case (NOP):
default:
	break;
	}
}
