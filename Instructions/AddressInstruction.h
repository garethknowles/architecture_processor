/*
 * AddressInstruction.h
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#ifndef ADDRESSINSTRUCTION_H_
#define ADDRESSINSTRUCTION_H_

#include <iostream>

using namespace std;

#include "Instruction.h"

class AddressInstruction: public Instruction {
public:
	string label;
	int address;
	AddressInstruction(instructionType type, string lab);
	AddressInstruction(AddressInstruction *org);
	virtual ~AddressInstruction();
	virtual void execute();
	virtual void setAddress(int address);
};

#endif /* ADDRESSINSTRUCTION_H_ */
