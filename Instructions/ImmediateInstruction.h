/*
 * ImmediateInstruction.h
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#ifndef IMMEDIATEINSTRUCTION_H_
#define IMMEDIATEINSTRUCTION_H_

#include "Instruction.h"

class ImmediateInstruction: public Instruction {
public:
	unsigned short  register_d_num;
	int register_d_val;
	unsigned short  register_s_num;
	int register_s_val;
	unsigned short immediate;

	ImmediateInstruction(instructionType type, unsigned short rd, unsigned short rs, unsigned short im) ;
	ImmediateInstruction(ImmediateInstruction *org);
	virtual ~ImmediateInstruction();
	virtual void execute();
};

#endif /* REGISTERINSTRUCTION_H_ */
