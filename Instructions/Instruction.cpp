/*
 * Instruction.cpp
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#include "Instruction.h"

Instruction::Instruction(instructionType itype) {
	this->type = itype;
	instructionMode = -1;
	updateReg = false;
	updateMem = false;
	updateAcc = false;
	updatePC = false;
	conditional = false;
	setGreaterThanFlag = false;
	setLessThanFlag = false;
	setNotEqualFlag = false;
	conditionalOnLT = false;
	contitionalOnNE = false;
	conditionalOnEQ = false;
	contitionalOnGT = false;
	branch = false;
	store_val = 0;
	store_pos = 0;
	load_val = 0;
	load_pos = 0;
	accu = 0;
	pcAtFetch = 0;
	issuedNumber = 0;
}


Instruction::Instruction(Instruction *org) {
	type = org->type;
	instructionMode = org->instructionMode;
	updateReg = org->updateReg;
	updateMem = org->updateMem;
	updateAcc = org->updateAcc;
	updatePC = org->updateAcc;
	setGreaterThanFlag = org->setGreaterThanFlag;
	setLessThanFlag = org->setLessThanFlag;
	setNotEqualFlag = org->setNotEqualFlag;
	conditionalOnLT = org->conditionalOnLT;
	contitionalOnNE = org->contitionalOnNE;
	contitionalOnGT = org->contitionalOnGT;
	conditionalOnEQ = org->conditionalOnEQ;
	conditional = org->conditional;
	store_val = org->store_val;
	store_pos = org->store_pos;
	load_val = 0;
	load_pos = 0;
	accu = org->accu;
	branch = org->branch;
	pcAtFetch = org->pcAtFetch;
	issuedNumber = org->issuedNumber;
}

Instruction::~Instruction() {
}

void Instruction :: execute() {
	// Does Nothing, override in Subclasses
}
