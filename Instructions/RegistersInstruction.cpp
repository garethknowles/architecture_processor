/*
 * RegistersInstruction.cpp
 *
 *  Created on: Oct 8, 2012
 *      Author: GSK
 */

#include "RegistersInstruction.h"

RegistersInstruction::RegistersInstruction(instructionType type,
		unsigned short rd, unsigned short rs, unsigned short rt) :
		Instruction(type) {
	instructionMode = 1;
	register_d_num = rd;
	register_d_val = 0;
	register_s_num = rs;
	register_s_val = 0;
	register_t_num = rt;
	register_t_val = 0;

}

RegistersInstruction::RegistersInstruction(RegistersInstruction *org) :
		Instruction(org) {
	instructionMode = org->instructionMode;
	register_d_num = org->register_d_num;
	register_d_val = org->register_d_val;
	register_s_num = org->register_s_num;
	register_s_val = org->register_s_val;
	register_t_num = org->register_t_num;
	register_t_val = org->register_t_val;
	issuedNumber = org->issuedNumber;
}

RegistersInstruction::~RegistersInstruction() {
}

void RegistersInstruction::execute() {
	int HI, LO;
	switch (this->type) {

	int cmp;

		// CONDITION
	case (COMPARE):
		cmp = (
				register_s_val == register_t_val ?
						0 : (register_s_val < register_t_val ? -1 : 1));
		setGreaterThanFlag = (cmp == 1 ? true : false);
		setLessThanFlag = (cmp == -1 ? true : false);
		setNotEqualFlag = (cmp == 0 ? false : true);
		if (register_d_num != 0) {
			store_pos = register_d_num;
			store_val = cmp;
			updateReg = true;
		}
		break;

		// ARITHMETIC
	case (ADD):
		register_d_val = register_s_val + register_t_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (SUB):
		register_d_val = register_s_val - register_t_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (MUL):
		store_val = (int)((long int) register_s_val * (long int) register_t_val);
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (MUL_ACCUMULATOR):
		accu = (long int) register_s_val * (long int) register_t_val;
		updateAcc = true;
		break;
	case (DIV):
		HI = register_s_val / register_t_val;
		LO = register_s_val % register_t_val;
		accu = (HI | LO);
		updateAcc = true;
		break;
	case (CLEAR):
		store_val = 0;
	 	store_pos = register_d_num;
	 	updateReg = true;
		break;
	case (SHIFT_LEFT):
		store_val = (int)(register_s_val << register_t_val);
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (SHIFT_RIGHT):
		store_val = (int)(register_s_val >> register_t_val);
		store_pos = register_d_num;
		updateReg = true;
		break;

		// DATA TRANSFER
	case (MOVE):
		register_d_val = register_s_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (MOVE_FROM_HIGH):
		register_d_val = (int) (accu >> 32);
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (MOVE_FROM_LOW):
		register_d_val = (int) accu;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;

		// LOGICAL
		break;
	case (AND):
		register_d_val = register_s_val & register_t_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (OR):
		register_d_val = register_s_val | register_t_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (EXCLUSIVE_OR):
		register_d_val = register_s_val ^ register_t_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (NOR):
		register_d_val = ~(register_s_val | register_t_val);
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (NOT):
		register_d_val = ~register_s_val;
		store_val = register_d_val;
		store_pos = register_d_num;
		updateReg = true;
		break;
	case (NOP) :
	default:
		break;
	}
}
