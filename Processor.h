/*
 * Processor.h
 *
 *  Created on: Nov 6, 2012
 *      Author: GSK
 */

#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#include "Components/RegisterFile.h"
#include "Components/MemoryFile.h"
#include "Components/FetchUnit.h"
#include "Components/DecodeUnit.h"
#include "Components/ReservationStation.h"
#include "Components/ExecutionUnit.h"
#include "Components/WritebackUnit.h"
#include "Benchmarks/Benchmarks.h"
#include "Components/Pipeline.h"
#include "Components/ReOrderUnit.h"
#include "Constants.h"
#include <vector>
#include <iostream>

using namespace std;

class Processor {

public:
	vector<FetchUnit*> fetchUnits;
	vector<DecodeUnit*> decUnits;
	vector<ExecutionUnit*> exeUnits;
	vector<WritebackUnit*> writeUnits;
	ReOrderUnit *reOrderUnit;
	ReservationStation *resStat;
	vector<Instruction*> *insList;
	RegisterFile *reg;
	MemoryFile *mem;
	Pipeline *pipeline;
	int count;
	int processorUnits;

	Processor();
	virtual ~Processor();

	int main();

	void runProcessor(vector<Instruction*> *instructions, RegisterFile *reg,
			MemoryFile *mem, ProcessorMode mode);
	void runProcessorNoPipe(vector<Instruction*> *instructions,
			RegisterFile *reg, MemoryFile *mem);
	void runProcessorScalar(vector<Instruction*> *instructions,
			RegisterFile *reg, MemoryFile *mem);
	void runProcessorSuperScalar(vector<Instruction*> *iList,
			RegisterFile *ireg, MemoryFile *imem);
	void runProcessorSuperScalarReOrder(vector<Instruction*> *iList,
			RegisterFile *ireg, MemoryFile *imem);
	bool step();

};

#endif /* PROCESSOR_H_ */
