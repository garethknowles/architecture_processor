/*
 * Benchmarks.cpp
 *
 *  Created on: Nov 30, 2012
 *      Author: GSK
 */

#include "Benchmarks.h"

Benchmarks::Benchmarks(Processor *p) {

	myProcessor = p;
}

Benchmarks::~Benchmarks() {
}

void Benchmarks::startDemoMode() {
	while (true) {
		// Run Demos forever
		cin.clear();
		cin.sync();
		cout << "===================================================" << endl;
		cout << "=========== Benchmark Demo Mode Started ===========" << endl;
		cout << "===================================================" << endl;
		cout << "Please Select a Benchmark from the following: " << endl;
		cout << "1 - Vector Addition" << endl;
		cout << "2 - Vector Addition Unrolled" << endl;
		cout << "3 - Bubble Sort" << endl;
		cout << "4 - Factorial" << endl;
		cout << "5 - Greatest Common Divisors" << endl;
		cout << "6 - Matrix Multiply" << endl;
		int bench = 1;
		cout << "Enter your choice number now:" << endl;
		cin.clear();
		cin.sync();
		cin >> bench;

		cout << "===================================================" << endl;
		cout << "Please Select a Processor Mode from the following: " << endl;
		cout << "1 - None Pipelined Basic Processor" << endl;
		cout << "2 - Pipelined Simple Scalar Processor" << endl;
		cout << "3 - Pipelined Super Scalar Processor" << endl;
		cout << "4 - Super Scalar With Re-Order Buffer Processor" << endl;

		int processor = BasicNoPipe;
		cout << "Enter your choice number now:" << endl;
		cin.clear();
		cin.sync();
		cin >> processor;

		if (processor == 4 || processor == 3) {
			cout << "==================================================="
					<< endl;
			cout
					<< "Please Select the Number of Fetch, Decode, Execute and Writeback Units in Your Processor: "
					<< endl;

			int units = 1;
			cout << "Enter your choice number now:" << endl;
			cin.clear();
			cin.sync();
			cin >> units;

			if (units < 1)
				units = 1;

			myProcessor->processorUnits = units;
		}

		ProcessorMode mode = BasicNoPipe;
		if (processor == 1)
			mode = BasicNoPipe;
		if (processor == 2)
			mode = BasicScalar;
		if (processor == 3)
			mode = BasicSuperScalar;
		if (processor == 4)
			mode = SuperScalarReOrder;

		cin.clear();
		cin.sync();

		if (bench == 1)
			runVectorAdditionBenchmark(mode);
		if (bench == 2)
			runVectorAdditionUnrolledBenchmark(mode);
		if (bench == 3)
			runBubbleSortBenchmark(mode);
		if (bench == 4)
			runFactorialBenchmark(mode);
		if (bench == 5)
			runGDCRecursiveBenchmark(mode);
		if (bench == 6)
			runMatrixMultiply(mode);
	}

}

void Benchmarks::runBasicTest(ProcessorMode mode) {

	// COMPILE CODE
	Assembly *a = new Assembly();
	ifstream *file = new ifstream("Benchmarks/basic_test");
	vector<Instruction*> instructionList = a->assemblyToInstructions(file,
			verboseAssembly);

	// SETUP
	RegisterFile *reg = new RegisterFile();
	// Make 300 memory ints for values
	MemoryFile *mem = new MemoryFile(10);

	mem->setVal(0, 2);
	mem->setVal(1, 2);

	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);
}

void Benchmarks::runVectorAdditionBenchmark(ProcessorMode mode) {

	// Setup to add vector a + b, where a = b = [1,2,3,...100], with c[100] holding the results

	// COMPILE CODE
	Assembly *a = new Assembly();
	ifstream *file = new ifstream("Benchmarks/vector_addition");
	vector<Instruction*> instructionList = a->assemblyToInstructions(file,
			verboseAssembly);

	// SETUP
	RegisterFile *reg = new RegisterFile();
	// Make 300 memory ints for values
	MemoryFile *mem = new MemoryFile(300);
	int max = 100;
	for (int i = 0; i < max; i++) {
		mem->setVal(i, i + 1);
		mem->setVal(i + max, i + 1);
	}

	cout << "Vector Addition Benchmark" << endl;
	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);
}

void Benchmarks::runVectorAdditionUnrolledBenchmark(ProcessorMode mode) {

	// Setup to add vector a + b, where a = b = [1,2,3,...100], with c[100] holding the results

	// COMPILE CODE
	Assembly *a = new Assembly();
	ifstream *file = new ifstream("Benchmarks/vector_addition_unrolled");
	vector<Instruction*> instructionList = a->assemblyToInstructions(file,
			verboseAssembly);

	// SETUP
	RegisterFile *reg = new RegisterFile();
	// Make 300 memory ints for values
	MemoryFile *mem = new MemoryFile(300);
	int max = 100;
	for (int i = 0; i < max; i++) {
		mem->setVal(i, i + 1);
		mem->setVal(i + max, i + 1);
	}

	cout << "Vector Addition Unrolled By 5 Benchmark" << endl;
	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);
}

void Benchmarks::runBubbleSortBenchmark(ProcessorMode mode) {
	// COMPILE CODE
	Assembly *a = new Assembly();
	ifstream *file = new ifstream("Benchmarks/bubble_sort");
	vector<Instruction*> instructionList = a->assemblyToInstructions(file,
			false);

	bool fixed = false;
	if (demoMode) {
		cout << "===================================================" << endl;
		cout << "Would You like your Bubble Sort Values to be Random: " << endl;
		cout << "1 - Psuedo-Randomly Generated" << endl;
		cout << "2 - Fixed Reverse Order Set (fixed runtime)" << endl;
		int rand = 1;
		cout << "Enter your choice number now:" << endl;
		cin.clear();
		cin.sync();
		cin >> rand;
		if (rand == 2) {
			fixed = true;
		}
	}

	// SETUP
	RegisterFile *reg = new RegisterFile();
	// Make 10 random values
	srand(time(NULL));
	MemoryFile *mem = new MemoryFile(20);
	if (fixed) {
		for (int i = 0; i < 20; i++) {
			mem->setVal(i, 20 - i);
		}
	} else {
		for (int i = 0; i < 20; i++) {
			mem->setVal(i, rand() % 100);
		}
	}

	cout << "Bubble Sort Benchmark" << endl;
	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);
}

void Benchmarks::runFactorialBenchmark(ProcessorMode mode) {
	// COMPILE CODE
	Assembly *a = new Assembly();
	ifstream *file = new ifstream("Benchmarks/factorial");
	vector<Instruction*> instructionList = a->assemblyToInstructions(file,
			verboseAssembly);


	int num = 12;

	// SETUP
	RegisterFile *reg = new RegisterFile();
	MemoryFile *mem = new MemoryFile(1);
	mem->setVal(0, num);

	cout << "Factorial Benchmark" << endl;
	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);
}

void Benchmarks::runGDCRecursiveBenchmark(ProcessorMode mode) {
	// COMPILE CODE
	Assembly *a = new Assembly();
	ifstream *file = new ifstream("Benchmarks/gdc_recursive");
	vector<Instruction*> instructionList = a->assemblyToInstructions(file,
			verboseAssembly);

	// SETUP

	int num1 = 279;
	int num2 = 27;

	RegisterFile *reg = new RegisterFile();
	MemoryFile *mem = new MemoryFile(3);
	if (demoMode) {
		cout << "===================================================" << endl;
		cout << "Please Enter Two Values to Find Their Greatest Common Divisor: " << endl;
		cout << "Enter your choice number now:" << endl;
		cin.clear();
		cin.sync();
		cin >> num1;
		cin >> num2;
	}
	mem->setVal(0, num1);
	mem->setVal(1, num2);

	cout << "GCD Benchmark - GCD(" << num1 << ", " << num2 << ")" << endl;

	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);

}

void Benchmarks::runMatrixMultiply(ProcessorMode mode) {
	// COMPILE CODE
	Assembly *ass = new Assembly();
	ifstream *file = new ifstream("Benchmarks/matrix_multiply");
	vector<Instruction*> instructionList = ass->assemblyToInstructions(file,
			verboseAssembly);

	// SETUP
	RegisterFile *reg = new RegisterFile();
	MemoryFile *mem = new MemoryFile(30);

	int matrixSize = 3;
	// Set Matrix Size
	mem->setVal(0, matrixSize);

	// Set Matrix Values
	int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	int b[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

	// {{1,2,3}, {4,5,6}, {7,8,9}}*{{9,8,7}, {6,5,4}, {3,2,1}} = {{30, 24, 18}, {84, 69, 54}, {138, 114, 90}}
	int start = matrixSize * matrixSize;
	for (int i = 0; i < matrixSize; i++) {
		for (int j = 0; j < matrixSize; j++) {
			int pos = (i * matrixSize) + j;
			mem->setVal(pos + start, a[pos]);
		}
	}
	start = start + start;
	for (int i = 0; i < matrixSize; i++) {
		for (int j = 0; j < matrixSize; j++) {
			int pos = (i * matrixSize) + j;
			mem->setVal(pos + start, b[pos]);
		}
	}

	// MEM: C[] A[] B[]
	// C[] = A[] x B[]

	cout << "Matrix Multiply Benchmark" << endl;
	// RUN!
	myProcessor->runProcessor(&instructionList, reg, mem, mode);
}
