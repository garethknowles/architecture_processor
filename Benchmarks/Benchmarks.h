/*
 * Benchmarks.h
 *
 *  Created on: Nov 30, 2012
 *      Author: GSK
 */

#ifndef BENCHMARKS_H_
#define BENCHMARKS_H_

#include "../Processor.h"
#include "../Components/RegisterFile.h"
#include "../Components/MemoryFile.h"
#include "../Components/ExecutionUnit.h"
#include <vector>
#include <iostream>
#include "../Assembly.h"
#include "../Constants.h"

class Processor;

class Benchmarks {
public:
	Processor *myProcessor;
	Benchmarks(Processor *p);
	virtual ~Benchmarks();

	void startDemoMode();

	void runVectorAdditionBenchmark(ProcessorMode mode);
	void runVectorAdditionUnrolledBenchmark(ProcessorMode mode);
	void runBubbleSortBenchmark(ProcessorMode mode);
	void runFactorialBenchmark(ProcessorMode mode);
	void runGDCRecursiveBenchmark(ProcessorMode mode);
	void runMatrixMultiply(ProcessorMode mode);
	void runBasicTest(ProcessorMode mode);
};

#endif /* BENCHMARKS_H_ */
