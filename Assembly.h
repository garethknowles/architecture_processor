/*
 * Assembly.h
 *
 *  Created on: Nov 8, 2012
 *      Author: GSK
 */

#ifndef ASSEMBLY_H_
#define ASSEMBLY_H_

#include "Instructions/Instruction.h"
#include "Instructions/AddressInstruction.h"
#include "Instructions/ImmediateInstruction.h"
#include "Instructions/RegistersInstruction.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <algorithm>

using namespace std;

enum conditionType {
	ConditionAlways,
	ConditionEqual,
	ConditionNotEqual,
	ConditionGreaterThanOrEqual,
	ConditionLessThanOrEqual,
	ConditionGreaterThan,
	ConditionLessThan,
};

struct InstructionData {
	string typeName;
	conditionType condition;
	int rd;
	int rs;
	int rt;
	int immediate;
	string label;
};

struct LabelLookup {
	string label;
	int address;
};

class Assembly {
public:
	Assembly();
	virtual ~Assembly();

	virtual vector<Instruction* > assemblyToInstructions( istream *input, bool verbose);
	virtual bool checkForPrefix( std::string const& lhs, std::string const& rhs );
	virtual InstructionData* extractInstructionDataFromString(string instructionString);
	virtual conditionType checkForConditionType(string *input);
	virtual void handleConditions(Instruction *ins, InstructionData *data);
};

#endif /* ASSEMBLY_H_ */
