################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Instructions/AddressInstruction.cpp \
../Instructions/ImmediateInstruction.cpp \
../Instructions/Instruction.cpp \
../Instructions/RegistersInstruction.cpp 

OBJS += \
./Instructions/AddressInstruction.o \
./Instructions/ImmediateInstruction.o \
./Instructions/Instruction.o \
./Instructions/RegistersInstruction.o 

CPP_DEPS += \
./Instructions/AddressInstruction.d \
./Instructions/ImmediateInstruction.d \
./Instructions/Instruction.d \
./Instructions/RegistersInstruction.d 


# Each subdirectory must supply rules for building sources it contributes
Instructions/%.o: ../Instructions/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


