################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Components/CallStackFrame.cpp \
../Components/DecodeUnit.cpp \
../Components/ExecutionUnit.cpp \
../Components/FetchUnit.cpp \
../Components/MemoryFile.cpp \
../Components/Pipeline.cpp \
../Components/ReOrderUnit.cpp \
../Components/RegisterFile.cpp \
../Components/ReservationStation.cpp \
../Components/WritebackUnit.cpp 

OBJS += \
./Components/CallStackFrame.o \
./Components/DecodeUnit.o \
./Components/ExecutionUnit.o \
./Components/FetchUnit.o \
./Components/MemoryFile.o \
./Components/Pipeline.o \
./Components/ReOrderUnit.o \
./Components/RegisterFile.o \
./Components/ReservationStation.o \
./Components/WritebackUnit.o 

CPP_DEPS += \
./Components/CallStackFrame.d \
./Components/DecodeUnit.d \
./Components/ExecutionUnit.d \
./Components/FetchUnit.d \
./Components/MemoryFile.d \
./Components/Pipeline.d \
./Components/ReOrderUnit.d \
./Components/RegisterFile.d \
./Components/ReservationStation.d \
./Components/WritebackUnit.d 


# Each subdirectory must supply rules for building sources it contributes
Components/%.o: ../Components/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


