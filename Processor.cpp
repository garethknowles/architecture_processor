/*
 * Processor.cpp
 *
 *  Created on: Nov 6, 2012
 *      Author: GSK
 */

#include "Processor.h"

Processor::Processor() {
	resStat = NULL;
	reOrderUnit = NULL;
	insList = NULL;
	reg = NULL;
	mem = NULL;
	pipeline = NULL;
	count = 0;
	processorUnits = 1;
}

Processor::~Processor() {
}

int main() {
	Processor *p = new Processor();
	Benchmarks *benchs = new Benchmarks(p);

	if (demoMode) {
		// DEMO MODE
		benchs->startDemoMode();
	} else {
		// DEBUG MODE
		//benchs->runBasicTest();
		//benchs->runBubbleSortBenchmark(BasicSuperScalar);
		//benchs->runBubbleSortBenchmark(BasicScalar);
		//benchs->runVectorAdditionBenchmark();
		benchs->runVectorAdditionBenchmark(SuperScalarReOrder);
		//benchs->runFactorialBenchmark();
		//benchs->runGDCRecursiveBenchmark(BasicNoPipe);
		//benchs->runGDCRecursiveBenchmark(BasicScalar);
		//benchs->runMatrixMultiply(BasicNoPipe);
		//benchs->runMatrixMultiply(BasicSuperScalar);
		//benchs->runMatrixMultiply(BasicScalar);
	}
}

void Processor::runProcessor(vector<Instruction*> *iList, RegisterFile *ireg,
		MemoryFile *imem, ProcessorMode mode) {
	switch (mode) {
	case BasicNoPipe:
		runProcessorNoPipe(iList, ireg, imem);
		break;
	case BasicScalar:
		runProcessorScalar(iList, ireg, imem);
		break;
	case BasicSuperScalar:
		runProcessorSuperScalar(iList, ireg, imem);
		break;
	case SuperScalarReOrder:
		runProcessorSuperScalarReOrder(iList, ireg, imem);
		break;
	default:
		break;
	}
}

void Processor::runProcessorNoPipe(vector<Instruction*> *iList,
		RegisterFile *ireg, MemoryFile *imem) {

	cout << "Running NoPipe Processor" << endl;

	insList = iList;
	mem = imem;
	reg = ireg;
	pipeline = new Pipeline(insList);

	// Catch Empty Pipeline
	if (insList->size() == 0)
		return;

	fetchUnits = vector<FetchUnit*>();
	decUnits = vector<DecodeUnit*>();
	exeUnits = vector<ExecutionUnit*>();
	writeUnits = vector<WritebackUnit*>();

	// Setup Processor Run
	fetchUnits.push_back(new FetchUnit(reg, pipeline));
	decUnits.push_back(new DecodeUnit(reg, mem, pipeline));
	exeUnits.push_back(new ExecutionUnit(pipeline));
	writeUnits.push_back(new WritebackUnit(reg, mem, pipeline));

	bool run = true;
	count = 0;

	cout << endl << "== Start Run ==" << endl;

	// Run Through Pipepline
	while (run) {
		Instruction *current;
		if ((this->reg->getProgramCounter() >= 0)
				&& ((unsigned) this->reg->getProgramCounter()
						< this->insList->size())) {
			current = this->insList->at(reg->getProgramCounter());
		}

		// Run All Stages (REVERSE TO STOP CHEATING / Emulate Delay between ticks)
		fetchUnits[0]->currentInstruction = current;
		fetchUnits[0]->doFetchStageNoPipe();

		decUnits[0]->currentInstruction = fetchUnits[0]->currentInstruction;
		decUnits[0]->doDecodeStageNoPipe();

		exeUnits[0]->currentInstruction = decUnits[0]->currentInstruction;
		if (exeUnits[0]->doExecutionStageNoPipe())
			break;

		writeUnits[0]->currentInstruction = exeUnits[0]->currentInstruction;
		writeUnits[0]->doWriteBackStageNoPipe();

		count++;
		this->reg->incrementProgramCounter();
	}

	cout << "== Finished Run ==" << endl << endl;
	cout << "Runtime (Clock Ticks): " << count << endl << endl;

	cout << "== End Prints ==" << endl;
	// Finish with prints
	reg->printReg();
	mem->printMem();
	cout << endl;
}

void Processor::runProcessorScalar(vector<Instruction*> *iList,
		RegisterFile *ireg, MemoryFile *imem) {

	cout << "Running Scalar Processor" << endl;
	insList = iList;
	mem = imem;
	reg = ireg;
	resStat = new ReservationStation(reg, mem);
	pipeline = new Pipeline(insList);
	pipeline->resStation = resStat;

	// Catch Empty Pipeline
	if (insList->size() == 0)
		return;

	fetchUnits = vector<FetchUnit*>();
	decUnits = vector<DecodeUnit*>();
	exeUnits = vector<ExecutionUnit*>();
	writeUnits = vector<WritebackUnit*>();

	// Setup Processor Run
	fetchUnits.push_back(new FetchUnit(reg, pipeline));
	decUnits.push_back(new DecodeUnit(reg, mem, pipeline));
	exeUnits.push_back(new ExecutionUnit(pipeline));
	writeUnits.push_back(new WritebackUnit(reg, mem, pipeline));

	bool run = true;
	count = 0;

	cout << endl << "== Start Run ==" << endl;

	// Run Through Pipepline
	while (run) {
		run = step();
	}

	int insCount = pipeline->instructionCount;

	cout << "== Finished Run ==" << endl;
	cout << "Runtime (Clock Ticks): " << count << endl;
	cout << "Instructions Executed: " << insCount << endl;
	cout << "Instructions/Clock: " << ((float) insCount / (float) count) << endl
			<< endl;
	cout << "== End Prints ==" << endl;

	// Finish with prints
	reg->printReg();
	mem->printMem();
	cout << endl;
}

void Processor::runProcessorSuperScalar(vector<Instruction*> *iList,
		RegisterFile *ireg, MemoryFile *imem) {

	cout << "Running Super Scalar Processor" << endl;
	insList = iList;
	mem = imem;
	reg = ireg;
	pipeline = new Pipeline(insList);
	resStat = new ReservationStation(reg, mem);
	pipeline->resStation = resStat;

	// Catch Empty Pipeline
	if (insList->size() == 0)
		return;

	fetchUnits = vector<FetchUnit*>();
	decUnits = vector<DecodeUnit*>();
	exeUnits = vector<ExecutionUnit*>();
	writeUnits = vector<WritebackUnit*>();

	// Setup Processor Run
	for (int i = 0; i < processorUnits; i++) {
		fetchUnits.push_back(new FetchUnit(reg, pipeline));
		decUnits.push_back(new DecodeUnit(reg, mem, pipeline));
		exeUnits.push_back(new ExecutionUnit(pipeline));
		writeUnits.push_back(new WritebackUnit(reg, mem, pipeline));
	}

	bool run = true;
	count = 0;

	cout << endl << "== Start Run ==" << endl;

	// Run Through Pipepline
	while (run) {
		run = step();
	}

	int insCount = pipeline->instructionCount;

	cout << "== Finished Run ==" << endl;
	cout << "Runtime (Clock Ticks): " << count << endl;
	cout << "Instructions Executed: " << insCount << endl;
	cout << "Instructions/Clock: " << ((float) insCount / (float) count) << endl
			<< endl;

	cout << "== End Prints ==" << endl;
	// Finish with prints
	reg->printReg();
	mem->printMem();
	cout << endl;
}

void Processor::runProcessorSuperScalarReOrder(vector<Instruction*> *iList,
		RegisterFile *ireg, MemoryFile *imem) {

	cout << "Running Super Scalar Processor" << endl;
	insList = iList;
	mem = imem;
	reg = ireg;
	pipeline = new Pipeline(insList);
	resStat = new ReservationStation(reg, mem);
	reOrderUnit = new ReOrderUnit(pipeline);
	resStat->reOrderUnit = reOrderUnit;
	pipeline->reOrderUnit = reOrderUnit;
	pipeline->resStation = resStat;

	// Catch Empty Pipeline
	if (insList->size() == 0)
		return;

	fetchUnits = vector<FetchUnit*>();
	decUnits = vector<DecodeUnit*>();
	exeUnits = vector<ExecutionUnit*>();
	writeUnits = vector<WritebackUnit*>();

	// Setup Processor Run
	for (int i = 0; i < processorUnits; i++) {
		fetchUnits.push_back(new FetchUnit(reg, pipeline));
		decUnits.push_back(new DecodeUnit(reg, mem, pipeline));
		exeUnits.push_back(new ExecutionUnit(pipeline));
		writeUnits.push_back(new WritebackUnit(reg, mem, pipeline));
	}

	bool run = true;
	count = 0;

	cout << endl << "== Start Run ==" << endl;

	// Run Through Pipepline
	while (run) {
		run = step();
	}

	int insCount = pipeline->instructionCount;

	cout << "== Finished Run ==" << endl;
	cout << "Runtime (Clock Ticks): " << count << endl;
	cout << "Instructions Executed: " << insCount << endl;
	cout << "Instructions/Clock: " << ((float) insCount / (float) count) << endl
			<< endl;

	cout << "== End Prints ==" << endl;
	// Finish with prints
	reg->printReg();
	mem->printMem();
	cout << endl;
}

bool Processor::step() {

// If nothing in pipeline at all, end!

// Run All Stages (REVERSE TO STOP CHEATING / Emulate Delay between ticks)
// Writeback - returns false on reOrder when hitting halt
	for (unsigned i = 0; i < writeUnits.size(); i++){
		if (writeUnits[i]->doWriteBackStage())
			return false;
	}
// ReOrderUnit - If on
	if (reOrderUnit != NULL)
		this->reOrderUnit->doReOrderStage();
// Execution
	for (unsigned i = 0; i < exeUnits.size(); i++) {
		// Check for halt
		if (exeUnits[i]->doExecutionStage()) {
			// For None reOrder Mode, return false on halt
			if (this->reOrderUnit == NULL)
				return false;
		}
	}
// Update Reservation Station
	pipeline->updateReservationStation();
// Decode
	for (unsigned i = 0; i < decUnits.size(); i++)
		decUnits[i]->doDecodeStage();
// Fetch
	for (unsigned i = 0; i < fetchUnits.size(); i++)
		fetchUnits[i]->doFetchStage();

//	this->writeUnit->doWriteBackStage();
//	if (this->exeUnit->doExecutionStage())
//		return false;
//	this->pipeline->updateReservationStation();
//	this->decUnit->doDecodeStage();
//	this->fetchUnit->doFetchStage();

	count++;
	return true;
}

